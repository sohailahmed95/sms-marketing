<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->longText('description')->nullable();
            $table->timestamp('start')->nullable();
            $table->timestamp('end')->nullable();
            
            $table->integer('status')->default(0);
            $table->integer('country_id')->unsigned();
            $table->integer('state_id')->unsigned();
            $table->integer('city_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('country_id')
                 ->references('id')->on('country')
                 ->onDelete('cascade');  

            $table->foreign('state_id')
                 ->references('id')->on('state')
                 ->onDelete('cascade');

            $table->foreign('city_id')
                 ->references('id')->on('city')
                 ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign');
    }
}

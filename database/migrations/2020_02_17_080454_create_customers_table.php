<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('registrant_name')->nullable();
            $table->string('registrant_address')->nullable();
            $table->string('registrant_phone')->nullable();
            $table->string('registrant_zip')->nullable();
            $table->string('registrant_email')->nullable();
            $table->integer('registrant_country_id')->unsigned();
            $table->integer('registrant_state_id')->unsigned();
            $table->integer('registrant_city_id')->unsigned();
            $table->integer('status')->default(0);
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('registrant_country_id')
                 ->references('id')->on('country')
                 ->onDelete('cascade');  

            $table->foreign('registrant_state_id')
                 ->references('id')->on('state')
                 ->onDelete('cascade');

            $table->foreign('registrant_city_id')
                 ->references('id')->on('city')
                 ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_sms', function (Blueprint $table) {
          $table->increments('id');
            $table->integer('campaign_id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->integer('sms_status_id')->unsigned();
            $table->longText('sms_exception')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('campaign_id')
                 ->references('id')->on('campaign')
                 ->onDelete('cascade');  
           
            $table->foreign('customer_id')
                 ->references('id')->on('customers')
                 ->onDelete('cascade');

            $table->foreign('sms_status_id')
                 ->references('id')->on('sms_status')
                 ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_sms');
    }
}

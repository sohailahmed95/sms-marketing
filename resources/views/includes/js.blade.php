<script  type="text/javascript">
	    function activate(id)
{
  var url = "{{ route('user_activate')}}";  
    $.ajax({
        type: "post",
        dataType: 'json',
        url: url,
        data: {
            id: id,
        },
        success: function(data) {
          location.reload();
     },
        error: function(data) {
            console.log(data);
        }
    });
}   

 function deactivate(id)
{
  var url = "{{ route('user_deactivate')}}";  
    $.ajax({
        type: "post",
        dataType: 'json',
        url: url,
        data: {
            id: id,
        },
        success: function(data) {
          location.reload();
     },
        error: function(data) {
            console.log(data);
        }
    });
}
</script>
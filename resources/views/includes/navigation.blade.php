<div id="app">
  <nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
      <!-- Toggler -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- Brand -->
      <a class="navbar-brand pt-0" href="{{ route('campaigns.index')}}">
        <h1>SMS Marketing</h1>
        {{-- <img src="./assets/img/brand/blue.png" class="navbar-brand-img" alt="..."> --}}
      </a>

      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Collapse header -->
        <div class="navbar-collapse-header d-md-none">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="./index.html">
                <img src="{{asset('/assets/img/brand/blue.png')}}">
              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <!-- Form -->
        <form class="mt-4 mb-3 d-md-none">
          <div class="input-group input-group-rounded input-group-merge">
            <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <span class="fa fa-search"></span>
              </div>
            </div>
          </div>
        </form>
        <!-- Navigation -->
        <ul class="navbar-nav">
      {{--     <li class="nav-item {{\Request::is('home') ? 'active' : ''}} ">
            <a class="nav-link  {{\Request::is('home') ? 'active' : ''}} " href="{{ route('home')}}">
              <i class="ni ni-tv-2 text-primary"></i> Dashboard
            </a>
          </li>   --}}   
      
        

          <li class="nav-item {{\Request::is('campaigns') ? 'active' : ''}}">
            <a class="nav-link {{\Request::is('campaigns') ? 'active' : ''}}" href="{{ route('campaigns.index')}}">
              <i class="ni ni-key-25 text-info"></i> Campaigns
            </a>
          </li>
          <li class="nav-item {{\Request::is('customers') ? 'active' : ''}}">
            <a class="nav-link {{\Request::is('customers') ? 'active' : ''}}" href="{{ route('customers.list')}}">
              <i class="ni ni-key-25 text-info"></i> Customers
            </a>
          </li> 

          <li class="nav-item {{\Request::is('sms') ? 'active' : ''}}">
            <a class="nav-link {{\Request::is('sms') ? 'active' : ''}}" href="{{ route('sms.index')}}">
              <i class="ni ni-key-25 text-info"></i> Send SMS
            </a>
          </li>
          
           <li class="nav-item {{\Request::is('users') ? 'active' : ''}}">
            <a class="nav-link {{\Request::is('users') ? 'active' : ''}} " href="{{ route('user.user_list')}}">
              <i class="ni ni-single-02 text-yellow"></i> Users
            </a>
          </li>
          <li class="nav-item {{\Request::is('settings') ? 'active' : ''}}">
            <a class="nav-link {{\Request::is('settings') ? 'active' : ''}}" href="{{ route('settings.index')}}">
              <i class="ni ni-key-25 text-info"></i> Settings
            </a>
          </li>

         {{--  <li class="nav-item">
            <a class="nav-link" href="{{ route('register')}}">
              <i class="ni ni-circle-08 text-pink"></i> Register
            </a>
          </li> --}}
        </ul>
        <!-- Divider -->
        {{-- <hr class="my-3"> --}}
        <!-- Heading -->
        {{-- <h6 class="navbar-heading text-muted">Documentation</h6> --}}
        <!-- Navigation -->
      {{--   <ul class="navbar-nav mb-md-3">
          <li class="nav-item">
            <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/getting-started/overview.html">
              <i class="ni ni-spaceship"></i> Getting started
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/foundation/colors.html">
              <i class="ni ni-palette"></i> Foundation
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/components/alerts.html">
              <i class="ni ni-ui-04"></i> Components
            </a>
          </li>
        </ul> --}}
     {{--    <ul class="navbar-nav">
          <li class="nav-item active active-pro">
            <a class="nav-link" href="./examples/upgrade.html">
              <i class="ni ni-send text-dark"></i> Upgrade to PRO
            </a>
          </li>
        </ul> --}}
      </div>
    </div>
  </nav>

  <div class="main-content">
 {{--    <div class="loaderOverlay">
    <div class="loader">
      <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="60px" height="60px" viewBox="0 0 128 128" xml:space="preserve"><g><linearGradient id="linear-gradient"><stop offset="0%" stop-color="#5e72e4" fill-opacity="1"/><stop offset="100%" stop-color="#32325d" fill-opacity="0.56"/></linearGradient><linearGradient id="linear-gradient2"><stop offset="0%" stop-color="#5e72e4" fill-opacity="1"/><stop offset="100%" stop-color="#32325d" fill-opacity="0.19"/></linearGradient><path d="M64 .98A63.02 63.02 0 1 1 .98 64 63.02 63.02 0 0 1 64 .98zm0 15.76A47.26 47.26 0 1 1 16.74 64 47.26 47.26 0 0 1 64 16.74z" fill-rule="evenodd" fill="url(#linear-gradient)"/><path d="M64.12 125.54A61.54 61.54 0 1 1 125.66 64a61.54 61.54 0 0 1-61.54 61.54zm0-121.1A59.57 59.57 0 1 0 123.7 64 59.57 59.57 0 0 0 64.1 4.43zM64 115.56a51.7 51.7 0 1 1 51.7-51.7 51.7 51.7 0 0 1-51.7 51.7zM64 14.4a49.48 49.48 0 1 0 49.48 49.48A49.48 49.48 0 0 0 64 14.4z" fill-rule="evenodd" fill="url(#linear-gradient2)"/><animateTransform attributeName="transform" type="rotate" from="0 64 64" to="360 64 64" dur="1600ms" repeatCount="indefinite"></animateTransform></g></svg>
    </div>
    </div> --}}
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        {{-- <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="{{ route('home')}}">SMS Marketing</a> --}}
        <!-- Form -->
      {{--   <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form> --}}
        <!-- User -->
        <ul class="navbar-nav align-items-center ml-auto">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img  alt="" src="{{asset('/assets/img/theme/team-1-800x800.jpg')}}">
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold">{{ Auth::user()->name }}</span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
           {{--    <div class=" dropdown-header noti-title">
                <h6 class="text-overflow m-0">Welcome!</h6>
              </div> --}}
       {{--        <a href="./examples/profile.html" class="dropdown-item">
                <i class="ni ni-single-02"></i>
                <span>My profile</span>
              </a>
              <a href="./examples/profile.html" class="dropdown-item">
                <i class="ni ni-settings-gear-65"></i>
                <span>Settings</span>
              </a>
              <a href="./examples/profile.html" class="dropdown-item">
                <i class="ni ni-calendar-grid-58"></i>
                <span>Activity</span>
              </a>
              <a href="./examples/profile.html" class="dropdown-item">
                <i class="ni ni-support-16"></i>
                <span>Support</span>
              </a> --}}
              <div class="drop{{-- d --}}own-divider"></div>
              <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item">
                <i class="ni ni-user-run"></i>
                <span>Logout</span>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>

    <!-- End Navbar -->
    <!-- Header -->

@extends('layouts.app')
@section('content')
<div class="header pt-md-8">
    <div class="container-fluid">
        <div class="header-body">
            
            <div class="row mt-5">
                <div class="col">
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>

                        @endif
                        @endforeach
                        
                    </div>
                    <div class="card bg-default shadow">
                        <div class="card-header bg-transparent border-0">
                            {{-- <h3 class="text-white mb-0">Campaigns</h3> --}}
                            <div class="row">
                                <div class="col-lg-6">
                                    
                                    <h3 class="text-white mb-0">API Settings</h3>
                                </div>
                                <div class="col-lg-6">
                                    <a onclick="open_model()" href="#" class="btn btn-primary btn-sm float-right">
                                    Add Account
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center  hover cell-border table-light table-flush" >
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Account SID</th>
                                        <th scope="col">Auth Token</th>
                                        <th scope="col">Twilio Registered #</th>
                                        {{-- <th scope="col">STATUS</th> --}}
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- @php(dd($settings)); --}}
                                  @foreach($settings as $setting)
                                  <tr>
                                      <td></td>
                                      <td>{{$setting->account_sid}}</td>
                                      <td>{{$setting->auth_token}}</td>
                                      <td>{{$setting->twilio_no}}</td>
                                      {{-- <td>{{$setting->status}}</td> --}}
                                      <td>
                                         {{--  @if($setting->status=='0')
                                            <a onclick="" ><svg width="25px" hieght="25px" aria-hidden="true" focusable="false" data-prefix="fad" data-icon="toggle-on" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-toggle-on fa-w-18 fa-3x"><g class="fa-group"><path fill="#ffffff" d="M384 384a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-secondary"></path><path fill="#ff0000" d="M384 64H192C86 64 0 150 0 256s86 192 192 192h192c106 0 192-86 192-192S490 64 384 64zm0 320a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-primary"></path></g></svg></a>
                                            @elseif($setting->status=='1')<a onclick=";" ><svg width="25px" hieght="25px" aria-hidden="true" focusable="false" data-prefix="fad" data-icon="toggle-off" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-toggle-off fa-w-18 fa-3x"><g class="fa-group"><path fill="#44c70e" d="M384 64H192C86 64 0 150 0 256s86 192 192 192h192c106 0 192-86 192-192S490 64 384 64zM192 384a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-secondary"></path><path fill="#ffffff" d="M192 384a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-primary"></path></g></svg></a>
                                            @endif --}}
                                          <form class="apiDelete" action="{{ route('settings.destroy',$setting->id)}}" method="post">
                                                {{ csrf_field() }}
                                          <button type="submit" class="btn btn-danger btn-sm" title="Remove"><svg width="20px" height="20px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-trash-alt fa-w-14 fa-2x"><path fill="currentColor" d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z" class=""></path></svg></button></form>
                                      </td>
                                  </tr>

                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                            <div class="row mt-5">
                                <div class="col-md-12">
                                    <h3>Active API:  <span class="text-success">{{$active_setting->twilio_no ?? null}}</span></h3>

                                    
                                </div>
                                <div class="col-md-12">
                                    <form id="changeAPI" action="{{route('settings.change_api')}}" >
                                        <div class="row">
                                        <div class="form-group col-md-7">
                                        <label class="form-control-label" for="active_twilio_number"><span class="text-white">Change Setting</span></label>

                                        <select class="form-control " id="active_twilio_number" onchange="" required name="active_twilio_number" >
                                            <option  value=""selected hidden>Select Twilio Number</option>
                                            @foreach ($settings as $setting)
                                           
                                            <option  value="{{$setting->twilio_no}}" >{{$setting->twilio_no}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                        <div class="form-group col-md-5">
                                            <input type="submit" class="btn btn-success mt-4" value="Change">
                                        </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                    <form id="api_settingsForm" enctype="multipart/form-data" action="{{route('settings.store')}}" >
                {{ csrf_field() }}
                <!-- Modal -->
                <div class="modal " id="api_settingsModal" tabindex="-1" role="dialog" aria-labelledby="customerinfo_importTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title" id="customerinfo_importTitle">Add New API</h2>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                        <div class="form-group ">
                            <div class="input-group ">
                               
                                <label for="auth_token" class="col-md-12 control-label">Enter Auth Token</label>
                                <input class="form-control" type="text" name="auth_token" id="auth_token" required aria-label="auth_token" placeholder="b29c9xxxxxxxxxxxxxxxxxxxx6964271">
                            </div>
                        </div>   

                         <div class="form-group ">
                            <div class="input-group ">
                              
                                <label for="account_sid" class="col-md-12 control-label">Enter Account SID</label>
                                <input class="form-control" type="text" name="account_sid" id="account_sid" required aria-label="account_sid" placeholder="AC94de299xxxxxxxxxxxxxxxxxxxx3834b">
                            </div>
                        </div> 
                       <div class="form-group ">
                            <div class="input-group ">
                                
                                <label for="twilio_no" class="col-md-12 control-label">Enter Twilio Registered #</label>
                                <input class="form-control" type="text" name="twilio_no" id="twilio_no" required aria-label="twilio_no" placeholder="+161xxxxx187">
                            </div>
                        </div> 
                   
                     

                            </div>
                            <div class="modal-footer">
                                   <div class="text-center">
                            <button type="submit" class="btn btn-success mt-4">Save</button>
                        </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" ></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js"></script>

<script>
    $(document).ready(function() {
     $('.apiDelete').on('submit',function(e){
        if(!confirm('Do you want to delete this item?')){
              e.preventDefault();
        }
      });
$('#api_settingsForm').validate();
$('#changeAPI').validate();

    });
    function open_model(){

            $('#api_settingsModal').modal({
    show: true
}); 

            
    }
</script>
@endsection
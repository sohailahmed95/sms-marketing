@extends('layouts.app')
@section('content')
<div class="container pt-8">
    <!-- Table -->
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8">
                <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                {{--         @if(isset($sms_exception))
                        <p class="alert alert-warning">{{ $sms_exception }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p> --}}
                        @endif
                        @endforeach
                        
                </div>
                
            <div class="card bg-white shadow rounded">
                <div class="card-body px-lg-5 py-lg-5">
                    <div class=" text-muted mb-4">
                        <h1>Start Campaign</h1>
                        <hr>
                    </div>
                    <form role="form"  method="POST" action="{{ route('sms.send') }}">
                        {{ csrf_field() }}
                                <h5 class="text-danger">*Messages will be send to active customers only</h5>

                        <div class="form-group ">
                            <div class="input-group ">
                                
                                <label for="title" class="col-md-12 control-label">Select Campaign</label>
                                <select class="form-control" id="campaign_id" onchange="get_message()" name="campaign_id" required>
                                       <option value="" hidden selected>Select campaign</option>
                               
                                    @foreach($campaigns as $campaign)
                             {{--        @php 
                                    dd($campaign);
                                    @endphp --}}
                                       <option value="{{ $campaign->id }}">{{ $campaign->title }}</option>
                                    @endforeach
                                </select>
                              {{--   <p id="text">
                                    
                                </p> --}}
                            </div>
                        </div> 
                         <div class="form-group ">
                            <div class="input-group ">
                                <label for="title" class="col-md-12 control-label">Message Details</label>
                          <div class="" name="text" id="text" style="padding: 15px 15px;"   placeholder="" disabled >Details to be send</div>
                               
                           </div>
                        </div>
                     {{--    <div class="form-group ">
                            <div class="input-group ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                </div>
                                <label for="title" class="col-md-12 control-label">Message</label>
                                <textarea class="form-control" id="message" name="message" rows="4" cols="50"></textarea>
                            </div>
                        </div> --}}
                        
                        <div class="text-center">
                            <button type="submit" class="btn btn-success mt-4">Start</button>
                        </div>
                    </form>
                </div>

            
            </div>
        </div>
    </div>
</div>

<script>
        function get_message()
    {
      var id =  $('#campaign_id').val();
    var url = "{{ route('campaigns.message')}}";
    $.ajax({
    type: "post",
    dataType: 'json',
    url: url,
    data: {
    _token: '{{csrf_token()}}',
    campaignId: id
    },
    success: function(data) {
                $.each(data,function(key, value) {
                    // console.log(value.description);
                    $('#text').html(value.description);
                });
    // console.log(data);

    // location.reload();
    },
    error: function(data) {
    console.log(data);
    }
    });
    }
</script>
@endsection
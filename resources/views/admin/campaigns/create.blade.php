@extends('layouts.app')
@section('content')
<div class="container pt-8">
    <!-- Table -->
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card bg-white shadow rounded">           
                <div class="card-body px-lg-5 py-lg-5">
                    <div class="text-muted ">
                        <h1>Create New Campaign</h1>
                        <hr>
                    </div>
                    <form role="form"  method="POST" action="{{ route('campaigns.store') }}">
                        {{ csrf_field() }}
               <div class="row">
               	
                        <div class="form-group col-md-12 ">
                            <label for="title" class="col-md-12 control-label">Title</label>
                            
                            <div class="input-group input-group-alternative ">
                                
                              
                                <input class="form-control" placeholder="Title" type="text" name="title"  id="title"  value="{{ old('title') }}">

                            </div>
                        </div>
               </div>
               <div class="row">
               	
                        <div class="form-group col-md-12">
                            <label for="title" class="col-md-12 control-label">Description</label>

                            <div class="input-group input-group-alternative ">
                                       <textarea class="form-control" name="description" id="description" placeholder="" >{{ old('description')}}</textarea>
                            </div>
                        </div>
               </div>

               <div class="row">

                       <div class="form-group col-md-4">
                            <label for="title" class="col-md-12 control-label">Country</label>
                            
                            <div class="input-group input-group-alternative">
                            
                                <select class="form-control" id="country" name="country">
                                    @foreach($countries as $country)
                                      @if(old('country') == $country->id)
                                        <option value="{{ $country->id }}" selected>{{ $country->name }}</option>
                                      @else
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                      @endif
                                    @endforeach
                                </select>
                           
                            </div>

                            
                        </div>


                        <div class="form-group col-md-4">
                            <label for="title" class="col-md-12 control-label">State</label>

                            <div class="input-group input-group-alternative">
                                <select class="form-control" id="state" name="state">

                                </select>
                            </div>
                        </div>
               	
                        <div class="form-group col-md-4">
                            <label for="title" class="col-md-12 control-label">City</label>

                            <div class="input-group input-group-alternative">
                                <select class="form-control" id="city" name="city">

                                </select>
                            </div>
                            
                        </div>
                        

                        
                  
               </div> 

                 <div class="row">
               	
                        <div class="form-group col-md-6">
                            <label for="title" class="col-md-12 control-label">Start Date</label>

                            <div class="input-group input-group-alternative">
                              
                                <input class="form-control" placeholder="Start Date"  name="start" id="start" required>
                           
                            </div>
                            
                        </div>
                        <div class="form-group col-md-6">
                            <label for="title" class="col-md-12 control-label">End Date</label>

                            <div class="input-group input-group-alternative">
                              
                                <input  class="form-control" id="end" name="end" placeholder="End Date" required>
                            </div>
                        </div>

                     
                  
               </div>
                        <div class="">
                            <button type="submit" class="btn btn-success mt-4">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>

{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" ></script> --}}
{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script> --}}
<script src="//cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>

<script type="text/javascript">

    $(document).ready(function(){
                  $( function() {
    var today = new Date();
// console.log(today  );
    $('#start').datetimepicker({
      format:'Y-m-d H:i:s',
       minDateTime : today
    });

    $('#end').datetimepicker({
      format:'Y-m-d H:i:s',
       minDateTime : today
      
    });

});

        //$( '#description' ).ckeditor();
        CKEDITOR.replace( 'description' );
        /*$('#startDate').datetimepicker();
        $('#endDate').datetimepicker();*/

        $("#country").change(function(){
            getstates();
        });

        $("#country").focus(function(){
            getstates();
        });

        $("#state").change(function(){
            getcities();
        });

        $("#state").focus(function(){
            getcities();
        });
    })

    function getstates()
    {
        var country=$('#country').val();

        /*if($('#city').val())
        {
            city=$('#city').val();
        }
        else
        {
            city="{{ old('city') }}";
        }*/
        
        //alert('{{ url("/getstates/") }}/'+country);

        $.ajax({
          url: '{{ url("/getstates/")}}/'+country,
          type: "get",
          dataType: 'json',
          success: function (data) {
             if(data.status==true){
               var options="";
               if(data.states.length > 0){        
                $.each(data.states, function(key,value) {
                    //if(value['state']=="{{ old('state') }}")
                    //options += "<option selected value="+value['name']+" >"+value['name']+"</option>";
                    //else
                    options += "<option value="+value['id']+" >"+value['name']+"</option>";    
                });

                $("#state").html("");    
                $("#state").append(options);      

               }
               else
               {
                  //alert('no data available');
               }
                
             }
             else
             {
                alert("Please select data");
             }
           
          },
          error: function (data) {
               alert('something wrong')
            } 
              
      });
     }

    function getcities()
    {
        var state=$('#state').val();

        /*if($('#city').val())
        {
            city=$('#city').val();
        }
        else
        {
            city="{{ old('city') }}";
        }*/
        
        //alert('{{ url("/getstates/") }}/'+country);

        $.ajax({
          url: '{{ url("/getcities/")}}/'+state,
          type: "get",
          dataType: 'json',
          success: function (data) {
             if(data.status==true){
               var options="";
               if(data.cities.length > 0){        
                $.each(data.cities, function(key,value) {
                    //if(value['state']=="{{ old('state') }}")
                    //options += "<option selected value="+value['name']+" >"+value['name']+"</option>";
                    //else
                    options += "<option value="+value['id']+" >"+value['name']+"</option>";    
                });

                $("#city").html("");    
                $("#city").append(options);      

               }
               else
               {
                  //alert('no data available');
               }
                
             }
             else
             {
                alert("Please select data");
             }
           
          },
          error: function (data) {
               alert('something wrong')
            } 
              
      });
     }   
   
     

</script>
@endsection
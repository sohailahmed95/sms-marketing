@extends('layouts.app')
@section('content')
<div class="header pt-md-8">
	<div class="container-fluid">
		<div class="header-body">
			
			<div class="row mt-5">
				<div class="col">
					<div class="flash-message">
						@foreach (['danger', 'warning', 'success', 'info'] as $msg)
						@if(Session::has('alert-' . $msg))
						<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>

                        @endif
						@endforeach
						
					</div>
					<div class="card bg-default shadow">
						<div class="card-header bg-transparent border-0">
							{{-- <h3 class="text-white mb-0">Campaigns</h3> --}}
							<div class="row">
								<div class="col-lg-6">
									
									<h3 class="text-white mb-0">Campaigns SMS Information</h3>
								</div>
								<div class="col-lg-6">
									<a href="{{ route('campaigns.index') }}" class="btn btn-primary btn-sm float-right">
									Back
									</a>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table align-items-center  hover cell-border table-light table-flush" id="statusTable" >
								<thead class="thead-dark">

										<tr class="topHead">
										<th scope="col">#<br><input class="IDfilter"  type="text" ></th>
										<th scope="col">CAMPAIGN<br><input class="CAMPAIGNfilter" data-order="1" type="text" ></th>
										<th scope="col">CUSTOMER NAME<br><input class="CUSTOMERfilter" id="startDate" data-order="2" type="text" ></th>
										<th scope="col">CUSTOMER NUMBER<br><input class="NUMBERfilter" id="endDate" data-order="3" type="text" ></th>
										<th scope="col">STATUS<br><input class="STATUSfilter" data-order="4" type="text" ></th>
										<th scope="col">DATE<br><input class="DATEfilter" id="date" data-order="5" type="text" ></th>
										<th scope="col">ERRORS<br></th>										
									</tr>
								</thead>
								<tbody>
								  @if(count($campaign_sms) > 0)
									@foreach($campaign_sms as $i=>$cs)
								
									<tr>
										<th scope="row">
											<div class="media align-items-center">
												<div class="media-body">
													<span class="mb-0 text-sm">{{ ++$i }}</span>
												</div>
											</div>
										</th>
										<td>{{isset($cs->campaign->title) ? $cs->campaign->title:'No Title' }}</td>
										<td>{{isset($cs->customer->registrant_name) ? $cs->customer->registrant_name:'N/A'}}</td>
										<td>{{isset($cs->customer->registrant_phone) ? $cs->customer->registrant_phone:'N/A'}}</td>
										
									{{-- <td>{{isset($cs->sms_status) && $cs->sms_status==3 ? 'SENT' :'NOT SENT'}}</td> --}}
							{{-- @php(dd($cs)); --}}
									<td>
										@if($cs->sms_status_id == 1)
										{{$cs->sms_status->status ?? "n/a"}}

										@elseif($cs->sms_status_id == 2)
										{{$cs->sms_status->status ?? "n/a"}}

										@elseif($cs->sms_status_id == 3)
										{{$cs->sms_status->status ?? "n/a"}}

										@elseif($cs->sms_status_id == 4)
										{{$cs->sms_status->status ?? "n/a"}}
								  @endif

									</td>
									<td>{{isset($cs->created_at) ? $cs->created_at :''}}</td>
									<td>{{isset($cs->sms_exception) ? $cs->sms_exception :''}}</td>
									
								
									</tr>
									@endforeach

								  @else
								    <tr>
								       <td colspan="4" style="text-align: center;">There is no Record</td>
								    </tr>
								  @endif

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
<script src="{{ asset('js/app.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" ></script>

{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" ></script> --}}
{{-- <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js"></script> --}}
<script>


	$(document).ready(function() {

		  	$('#date').datetimepicker({
  		format:'Y-m-d',
  		timepicker:false,
  		allowBlank:true,
	validateOnBlur:false,
  	});


	var dtable =  $('#statusTable').DataTable({
	    sDom: 'lrtip',
	    "bSort" : false,
	      "pagingType": "full_numbers",
	      "processing": true,
          "scrollX": true,
	      "autoWidth": false,



	});
			 $('.CAMPAIGNfilter').on('keyup', function() {
    if (this.value.length) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
    if (!$(this).val()) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
});		

	 $('.CUSTOMERfilter').on('keyup', function() {
    if (this.value.length) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
    if (!$(this).val()) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
});	

 $('.NUMBERfilter').on('keyup', function() {
    if (this.value.length) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
    if (!$(this).val()) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
});

 $('.STATUSfilter').on('keyup', function() {
    if (this.value.length) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
    if (!$(this).val()) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
});
 

 	   $('.DATEfilter').on('change', function() {
      	if(this.value.length){
        var date = new Date(this.value);        
        let formatted_date = date.getFullYear() + "-" + ('0'+(date.getMonth()+1)).slice(-2) + "-"+("0" + date.getDate()).slice(-2);
     
       dtable.column($(this).attr('data-order')).search(formatted_date, true);
        dtable.draw();
    }
    if (!$(this).val()) {
       dtable.column($(this).attr('data-order')).search('', true);
       dtable.draw();
   }
});


});

</script>
@endsection
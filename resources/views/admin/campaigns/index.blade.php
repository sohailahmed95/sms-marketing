@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

<div class="header pt-md-8">
	<div class="container-fluid">
		<div class="header-body">
			
			<div class="row mt-5">
				<div class="col">
					<div class="flash-message">
						@foreach (['danger', 'warning', 'success', 'info'] as $msg)
						@if(Session::has('alert-' . $msg))
						<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
						@endif
						@endforeach
						
					</div>
					<div class="card bg-default shadow">
						<div class="card-header bg-transparent border-0">
							{{-- <h3 class="text-white mb-0">Campaigns</h3> --}}
							<div class="row">
								<div class="col-lg-6">
									
									<h3 class="text-white mb-0">Campaigns Information</h3>
								</div>
								<div class="col-lg-6">
									<a href="{{ route('campaigns.create') }}" class="btn btn-primary btn-sm float-right">
									Add New
									</a>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table align-items-center table-light table-flush" id="campaignTable">
								<thead class="thead-dark">
									<tr class="topHead">
										<th scope="col">#<br><input class="IDfilter"  type="text" ></th>
										<th scope="col">TITLE<br><input class="TITLEfilter" data-order="1" type="text" ></th>
										<th scope="col">START DATE<br><input class="STARTfilter" id="startDate" data-order="2" type="text" ></th>
										<th scope="col">END DATE<br><input class="ENDfilter" id="endDate" data-order="3" type="text" ></th>
										<th scope="col">COUNTRY<br><input class="COUNTRYfilter" data-order="4" type="text" ></th>
										<th scope="col">STATE<br><input class="STATEfilter" data-order="5" type="text" ></th>
										<th scope="col">CITY<br><input class="CITYfilter" data-order="6" type="text" ></th>
										<th scope="col">STATUS<br><input class="STATUSfilter" data-order="7" type="text" ></th>
										<th scope="col">ACTION
										</th>
									</tr>

								</thead>
								<tbody>
									@foreach($campaigns as $campaign)
									<tr>
										<th scope="row">
											<div class="media align-items-center">
												{{--    <a href="#" class="avatar rounded-circle mr-3">
													<img alt="Image placeholder" src="../assets/img/theme/bootstrap.jpg">
												</a> --}}
												<div class="media-body">
													<span class="mb-0 text-sm">{{ $campaign->id }}</span>
												</div>
											</div>
										</th>
										
										{{-- @php(dd(Carbon\Carbon::now()->toDateTimeString())) --}}

										<td>{{isset($campaign->title) ? $campaign->title:'N/A' }}</td>
										<td class="{{$campaign->start < Carbon\Carbon::now()->toDateTimeString() ? 'text-warning font-weight-bold' : ''}}">{{isset($campaign->start) ? $campaign->start:'N/A'}}</td>

										<td class="{{$campaign->end < Carbon\Carbon::now()->toDateTimeString() ? 'text-warning font-weight-bold' : ''}}">{{isset($campaign->end) ? $campaign->end:'N/A'}}</td>
										
										<td>{{isset($campaign->getcountry->name) ? $campaign->getcountry->name:'N/A'}}</td>
										<td>{{isset($campaign->getstate->name) ? $campaign->getstate->name:'N/A'}}</td>
										<td>{{isset($campaign->getcity->name) ? $campaign->getcity->name:'N/A'}}</td>
										
										
										<td>
										@if(($campaign->end >= Carbon\Carbon::now()->toDateTimeString()) && ($campaign->start >=Carbon\Carbon::now()->toDateTimeString()))
												@if($campaign->status == 0)
												<span>Inactive
												</span>
												@else
												<span>Active</span>
												@endif

										@else
										<span></span>
										@endif
													
										</td>
										<td class="">
											<a  title="Show Message" onclick="show_message('{{ $campaign->id }}')">
												
											<svg aria-hidden="true"  width="20px" hieght="20px" focusable="false" data-prefix="far" data-icon="sticky-note" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-sticky-note fa-w-14 fa-2x"><path fill="currentColor" d="M448 348.106V80c0-26.51-21.49-48-48-48H48C21.49 32 0 53.49 0 80v351.988c0 26.51 21.49 48 48 48h268.118a48 48 0 0 0 33.941-14.059l83.882-83.882A48 48 0 0 0 448 348.106zm-128 80v-76.118h76.118L320 428.106zM400 80v223.988H296c-13.255 0-24 10.745-24 24v104H48V80h352z" class=""></path></svg>
											</a>

											@if($campaign->status == 0)
											<a class="{{$campaign->start < Carbon\Carbon::now()->toDateTimeString() ? 'd-none' : ''}}" title="Campaign Status" onclick="status_update('{{ $campaign->id }}');" ><svg width="25px" hieght="25px" aria-hidden="true" focusable="false" data-prefix="fad" data-icon="toggle-on" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-toggle-on fa-w-18 fa-3x"><g class="fa-group"><path fill="#ffffff" d="M384 384a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-secondary"></path><path fill="#ff0000" d="M384 64H192C86 64 0 150 0 256s86 192 192 192h192c106 0 192-86 192-192S490 64 384 64zm0 320a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-primary"></path></g></svg></a>
											@elseif($campaign->status == 1)
											<a class="{{$campaign->start < Carbon\Carbon::now()->toDateTimeString() ? 'd-none' : ''}}" title="Campaign Status" onclick="status_update('{{$campaign->id }}');" ><svg width="25px" hieght="25px" aria-hidden="true" focusable="false" data-prefix="fad" data-icon="toggle-off" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-toggle-off fa-w-18 fa-3x"><g class="fa-group"><path fill="#44c70e" d="M384 64H192C86 64 0 150 0 256s86 192 192 192h192c106 0 192-86 192-192S490 64 384 64zM192 384a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-secondary"></path><path fill="#ffffff" d="M192 384a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-primary"></path></g></svg></a>
											@endif
											<a title="Edit Campaign" href="{{route('campaigns.edit',$campaign->id)}}">
												
											<svg aria-hidden="true" width="20px" height="20px" focusable="false" data-prefix="fas" data-icon="edit" class="svg-inline--fa fa-edit fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z"></path></svg>
											</a>

											<a title="Campaign Info" href="{{route('campaigns.smsview',$campaign->id)}}">
												

											<svg aria-hidden="true" width="20px" height="20px" focusable="false" data-prefix="far" data-icon="file-chart-line" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-file-chart-line fa-w-12 fa-2x"><path fill="#172b4d" d="M131.2 320h-22.4c-6.4 0-12.8 6.4-12.8 12.8v70.4c0 6.4 6.4 12.8 12.8 12.8h22.4c6.4 0 12.8-6.4 12.8-12.8v-70.4c0-6.4-6.4-12.8-12.8-12.8zm72-64h-22.4c-6.4 0-12.8 6.4-12.8 12.8v134.4c0 6.4 6.4 12.8 12.8 12.8h22.4c6.4 0 12.8-6.4 12.8-12.8V268.8c0-6.4-6.4-12.8-12.8-12.8zm49.6 160h22.4c6.4 0 12.8-6.4 12.8-12.8V300.8c0-6.4-6.4-12.8-12.8-12.8h-22.4c-6.4 0-12.8 6.4-12.8 12.8v102.4c0 6.4 6.4 12.8 12.8 12.8zM369.83 97.98L285.94 14.1c-9-9-21.2-14.1-33.89-14.1H47.99C21.5.1 0 21.6 0 48.09v415.92C0 490.5 21.5 512 47.99 512h287.94c26.5 0 48.07-21.5 48.07-47.99V131.97c0-12.69-5.17-24.99-14.17-33.99zM255.95 51.99l76.09 76.08h-76.09V51.99zM336 464.01H47.99V48.09h159.97v103.98c0 13.3 10.7 23.99 24 23.99H336v287.95z" class=""></path></svg>

											</a>
										</td>
										
									</tr>
									@endforeach
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="modal " id="campaignMessageModal" tabindex="-1" role="dialog" aria-labelledby="campaignMessageModal" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h2 class="modal-title" id="campaignMessageModal"> </h2>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
				
								<div class="form-group">

									<p id="campaign_message"></p>
								

							</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
{{-- <script src="{{ asset('js/app.js') }}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" ></script>

{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.js"></script> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" ></script> --}}
{{-- <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js"></script> --}}
<script>

	
$(document).ready( function () {

	// $("#desc").change(function () {
  
 // function status_update(id)
	// {

 //    t.column( 8 ).data()
 //       .each( function ( value, index ) {
 //           console.log( 'Data in index: '+index+' is: '+value );
 //           var tr = $('#example tbody tr:eq(' + index + ') td:eq(8)').html( '4' );
 
 //           t.column( '8' ).cells().invalidate().render();
 //       } );
 //       // } );

 //   }

		var dtable =  $('#campaignTable').DataTable({
	    sDom: 'lrtip',
	    "bSort" : false,
	      "pagingType": "full_numbers",
	      "processing": true,
          "scrollX": true,
	      "autoWidth": false,



	});
  // $( function() {
  	$('#startDate').datetimepicker({
  		format:'Y-m-d',
  		timepicker:false,
		allowBlank:true,
		validateOnBlur:false,
  	});

  	$('#endDate').datetimepicker({
  		format:'Y-m-d',
  		timepicker:false,
  		allowBlank:true,
	validateOnBlur:false,
  	});

// });


	 $('.TITLEfilter').on('keyup', function() {
    if (this.value.length) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
    if (!$(this).val()) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
});
	 $('.COUNTRYfilter').on('keyup', function() {
    if (this.value.length) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
    if (!$(this).val()) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
});
	 $('.STATEfilter').on('keyup', function() {
    if (this.value.length) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
    if (!$(this).val()) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
});
	 $('.CITYfilter').on('keyup', function() {
    if (this.value.length) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
    if (!$(this).val()) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
});
	 $('.STATUSfilter').on('keyup', function() {
    if (this.value.length) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
    if (!$(this).val()) {
        dtable.column($(this).attr('data-order')).search(this.value, true);
        dtable.draw();
    }
});

	   $('.STARTfilter').on('change', function() {
      	if(this.value.length){
        var date = new Date(this.value);
        // var year = date.getFullYear() ? date.getFullYear() : null ;
        // console.log(date.getFullYear());
        
        let formatted_date = date.getFullYear() + "-" + ('0'+(date.getMonth()+1)).slice(-2) + "-"+("0" + date.getDate()).slice(-2);

        // console.log(formatted_date);
      
       dtable.column($(this).attr('data-order')).search(formatted_date, true);
        dtable.draw();
    }
    if (!$(this).val()) {
       dtable.column($(this).attr('data-order')).search('', true);
       dtable.draw();
   }
});

	
	   $('.ENDfilter').on('change', function() {
      if(this.value.length){
        var date = new Date(this.value);
        // console.log(('0'+(date.getMonth()+1)).slice(-2).length);
        let formatted_date = date.getFullYear() + "-" + ('0'+(date.getMonth()+1)).slice(-2) + "-"+("0" + date.getDate()).slice(-2);
        // console.log(formatted_date);
        dtable.column($(this).attr('data-order')).search(formatted_date, true);
        dtable.draw();
    }
    if (!$(this).val()) {
       dtable.column($(this).attr('data-order')).search('', true);
       dtable.draw();
   }
});


});
	function status_update(id)
	{

		var url = "{{ route('campaigns.status')}}";
		$.ajax({
			type: "post",
			dataType: 'json',
			url: url,
			data: 
			{
				_token: '{{csrf_token()}}',
				id: id
			},
			success: function(data) {
				location.reload();
			},
			error: function(data) {
				console.log(data);
			}
		});
	}

	function show_message(id)
	{
	$('.modal-title').html('Message Detail - ');
	$('#campaignMessageModal').modal('show');

		var url = "{{ route('campaigns.show_message')}}";
		$.ajax({
			type: "post",
			dataType: 'json',
			url: url,
			data: 
			{
				_token: '{{csrf_token()}}',
				campaignID: id
			},
			success: function(data) {
				$('#campaign_message').html(data.data.description);
				$('.modal-title').append(data.data.title);

			},
			error: function(data) {
				console.log(data);
			}
		});
	}
</script>
@endsection
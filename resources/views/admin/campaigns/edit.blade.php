@extends('layouts.app')
@section('content')
<div class="container pt-8">
    <!-- Table -->
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card bg-white shadow rounded">           
                <div class="card-body px-lg-5 py-lg-5">
                    <div class="text-muted ">
                        <h1>Edit Campaign</h1>
                        <hr>
                    </div>
                    <form role="form"  method="POST" action="{{ route('campaigns.update') }}">
                        {{ csrf_field() }}
               <div class="row">
               	
                        <div class="form-group col-md-12 ">
                            <label for="title" class="col-md-12 control-label">Title</label>
                            
                            <div class="input-group input-group-alternative ">
                                
                                <input type="hidden" value="{{ $campaigns->id }}" name="id" id="id">
                                <input class="form-control" placeholder="Title" type="text" name="title"  id="title"  value="{{ old('title',$campaigns->title) }}" required>

                            </div>
                        </div>
               </div>
               <div class="row">
               	
                        <div class="form-group col-md-12">
                            <label for="title" class="col-md-12 control-label">Description</label>

                            <div class="input-group input-group-alternative ">
                                       <textarea class="form-control" name="description" id="description" cols="30" rows="2" placeholder="" required>{{ old('description',$campaigns->description)}}</textarea>
                            </div>
                        </div>
               </div>

               <div class="row">

                       <div class="form-group col-md-4">
                            <label for="title" class="col-md-12 control-label">Country</label>
                            
                            <div class="input-group input-group-alternative">
                            
                                <select class="form-control" id="country_id" name="country_id" required>
                                    @foreach($countries as $country)
                                      {{-- @if($campaigns->country_id == $country->id) --}}
                                        <option value="{{  old('country',$country->id) }}" {{$campaigns->country_id == $country->id ? 'selected' : ''}}>{{ $country->name }}</option>
                                      {{-- @else --}}
                                        {{-- <option value="{{  old('country',$country->id)}}">{{ $country->name }}</option> --}}
                                      {{-- @endif --}}
                                    @endforeach
                                </select>
                           
                            </div>

                            
                        </div>


                        <div class="form-group col-md-4">
                            <label for="title" class="col-md-12 control-label">State</label>

                            <div class="input-group input-group-alternative">
                                <select class="form-control" id="state_id" name="state_id" required>
                                    @foreach($states as $state)
                                      {{-- @if($campaigns->state_id == $state->id) --}}
                                        <option value="{{ old('state',$state->id) }}" {{$campaigns->state_id == $state->id ? 'selected' : ''}}>{{ $state->name }}</option>
                                      {{-- @else --}}
                                        {{-- <option value="{{ old('state',$state->id)  }}">{{ $state->name }}</option> --}}
                                      {{-- @endif --}}
                                    @endforeach
                                </select>
                            </div>
                        </div>
               	
                        <div class="form-group col-md-4">
                            <label for="title" class="col-md-12 control-label">City</label>

                            <div class="input-group input-group-alternative">
                                <select class="form-control" id="city_id" name="city_id" required>
                                    @foreach($cities as $city)
                                      {{-- @if($campaigns->city_id == $city->id) --}}
                                        <option value="{{ old('city',$city->id) }}" {{$campaigns->city_id == $city->id ? 'selected' : ''}}>{{ $city->name }}</option>
                                      {{-- @else --}}
                                        {{-- <option value="{{ old('city',$city->id) }}">{{ $city->name }}</option> --}}
                                      {{-- @endif --}}
                                    @endforeach
                                </select>
                            </div>
                            
                        </div>
                        

                        
                  
               </div> 

                 <div class="row">
               	{{-- @php(dd($campaigns)); --}}
                        <div class="form-group col-md-6">
                            <label for="title" class="col-md-12 control-label">Start Date</label>

                            <div class="input-group input-group-alternative">
                              
                                <input class="form-control" placeholder="Start Date"  name="start" id="start" value="{{ old('start',$campaigns->start)}}"  required>
                           
                            </div>
                            
                        </div>
                        <div class="form-group col-md-6">
                            <label for="title" class="col-md-12 control-label">End Date</label>

                            <div class="input-group input-group-alternative">
                                <input class="form-control" id="end" name="end" value="{{ old('end',$campaigns->end)}}" placeholder="End Date" required>
                            </div>
                        </div>

               </div>
                        <div class="">
                            <button type="submit" class="btn btn-success mt-4">Edit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>

{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" ></script> --}}
{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script> --}}
<script src="//cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>

<script type="text/javascript">

    $(document).ready(function(){
          $( function() {
    $('#start').datetimepicker({
      format:'Y-m-d H:i:s',
    });

    $('#end').datetimepicker({
      format:'Y-m-d H:i:s',
    });

});

        //$( '#description' ).ckeditor();
        CKEDITOR.replace( 'description' );
        /*$('#startDate').datetimepicker();
        $('#endDate').datetimepicker();*/

        $("#country_id").change(function(){
            getstates();
        });

        // $("#country_id").focus(function(){
        //     getstates();
        // });

        $("#state_id").change(function(){
            getcities();
        });

        // $("#state_id").focus(function(){
        //     getcities();
        // });
    })

    function getstates()
    {
        var country=$('#country_id').val();

        /*if($('#city').val())
        {
            city=$('#city').val();
        }
        else
        {
            city="{{ old('city') }}";
        }*/
        
        //alert('{{ url("/getstates/") }}/'+country);

        $.ajax({
          url: '{{ url("/getstates/")}}/'+country,
          type: "get",
          dataType: 'json',
          success: function (data) {
             if(data.status==true){
               var options="";
               if(data.states.length > 0){        
                $.each(data.states, function(key,value) {
                    //if(value['state']=="{{ old('state') }}")
                    //options += "<option selected value="+value['name']+" >"+value['name']+"</option>";
                    //else
                    options += "<option value="+value['id']+" >"+value['name']+"</option>";    
                });

                $("#state_id").html("");    
                $("#state_id").append(options);      

               }
               else
               {
                  //alert('no data available');
               }
                
             }
             else
             {
                alert("Please select data");
             }
           
          },
          error: function (data) {
               alert('something wrong')
            } 
              
      });
     }

    function getcities()
    {
        var state=$('#state_id').val();

        /*if($('#city').val())
        {
            city=$('#city').val();
        }
        else
        {
            city="{{ old('city') }}";
        }*/
        
        //alert('{{ url("/getstates/") }}/'+country);

        $.ajax({
          url: '{{ url("/getcities/")}}/'+state,
          type: "get",
          dataType: 'json',
          success: function (data) {
             if(data.status==true){
               var options="";
               if(data.cities.length > 0){        
                $.each(data.cities, function(key,value) {
                    //if(value['state']=="{{ old('state') }}")
                    //options += "<option selected value="+value['name']+" >"+value['name']+"</option>";
                    //else
                    options += "<option value="+value['id']+" >"+value['name']+"</option>";    
                });

                $("#city_id").html("");    
                $("#city_id").append(options);      

               }
               else
               {
                  //alert('no data available');
               }
                
             }
             else
             {
                alert("Please select data");
             }
           
          },
          error: function (data) {
               alert('something wrong')
            } 
              
      });
     }   
   
     

</script>
@endsection
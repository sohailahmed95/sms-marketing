@extends('layouts.app')
@section('content')
    <div class="header pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
				        <div class="col-md-12">
                <!-- Dark table -->
      <div class="row mt-5">
        <div class="col">
          <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
              <div class="row">
                <div class="col-lg-6">
                  
              <h3 class="text-white mb-0">Users</h3>
                </div>
                <div class="col-lg-6">

              <a href="{{ route('register') }}" class="btn btn-primary btn-sm text-light float-right"><b>Add New User</b></a>
              </div>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-dark table-flush">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">NAME</th>
                    <th scope="col">EMAIL</th>
                    <th scope="col">ROLE</th>
                    <th scope="col">STATUS</th>
                    <th scope="col">ACTION</th>
                    {{-- <th scope="col"></th> --}}
                  </tr>
                </thead>
                <tbody>
                     @foreach($users as $user)
                  <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                     {{--    <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="../assets/img/theme/bootstrap.jpg">
                        </a> --}}
                        <div class="media-body">
                          <span class="mb-0 text-sm">{{$user->id }}</span>
                        </div>
                      </div>
                    </th>
                    <td>
                    {{$user->name }}
                    </td>
                    <td>
                      {{-- <span class="badge badge-dot mr-4"> --}}
                       {{$user->email}}
                      {{-- </span> --}}
                    </td>
                    <td>
                         @if($user->is_admin =='0')
                                <span>User 
                                </span>
                                @else
                                <span>Admin</span>
                                @endif
                                 </td>
                    {{--   <div class="avatar-group">
                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="Ryan Tompson">
                          <img alt="Image placeholder" src="../assets/img/theme/team-1-800x800.jpg" class="rounded-circle">
                        </a>
                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="Romina Hadid">
                          <img alt="Image placeholder" src="../assets/img/theme/team-2-800x800.jpg" class="rounded-circle">
                        </a>
                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="Alexander Smith">
                          <img alt="Image placeholder" src="../assets/img/theme/team-3-800x800.jpg" class="rounded-circle">
                        </a>
                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="Jessica Doe">
                          <img alt="Image placeholder" src="../assets/img/theme/team-4-800x800.jpg" class="rounded-circle">
                        </a>
                      </div> --}}
                   
                    {{-- <td> --}}
                      <td>
                       @if($user->is_active=='0' && $user->is_admin =='0') <span>Inactive </span>                                      
                      @elseif($user->is_active=='1' && $user->is_admin =='0') <span>Active</span> @endif
                         </td>       
                      {{-- <div class="d-flex align-items-center"> --}}

                      {{--   <span class="mr-2">60%</span>
                        <div>
                          <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div>
                        </div> --}}
                      {{-- </div> --}}
                    {{-- </td> --}}
                    <td class="">
                      @if($user->is_active=='0' && $user->is_admin =='0')
                                    <a onclick="status_update('{{$user->id }}');" ><svg width="25px" hieght="25px" aria-hidden="true" focusable="false" data-prefix="fad" data-icon="toggle-on" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-toggle-on fa-w-18 fa-3x"><g class="fa-group"><path fill="#ffffff" d="M384 384a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-secondary"></path><path fill="#ff0000" d="M384 64H192C86 64 0 150 0 256s86 192 192 192h192c106 0 192-86 192-192S490 64 384 64zm0 320a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-primary"></path></g></svg></a>

                                    @elseif($user->is_active=='1' && $user->is_admin =='0')<a onclick="status_update('{{$user->id }}');" ><svg width="25px" hieght="25px" aria-hidden="true" focusable="false" data-prefix="fad" data-icon="toggle-off" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-toggle-off fa-w-18 fa-3x"><g class="fa-group"><path fill="#44c70e" d="M384 64H192C86 64 0 150 0 256s86 192 192 192h192c106 0 192-86 192-192S490 64 384 64zM192 384a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-secondary"></path><path fill="#ffffff" d="M192 384a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-primary"></path></g></svg></a>
                                     @endif
                    </td>
                  </tr>
                                         @endforeach

                
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
                </div>
            </div>
        </div>
          </div>
        </div>
      </div>
    </div>
   
  </div>
</div>
<script>
function status_update(id)
{
  var url = "{{ route('user.status_update')}}";
    $.ajax({
        type: "post",
        dataType: 'json',
        url: url,
        data: {
            _token: '{{csrf_token()}}',
            userId: id
        },
        success: function(data) {
          location.reload();
     },
        error: function(data) {
            console.log(data);
        }
    });
}
</script>
@endsection
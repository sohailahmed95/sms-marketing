@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="container pt-8">
    <!-- Table -->
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card bg-white shadow rounded">           
                <div class="card-body px-lg-5 py-lg-5">
                    <div class="text-muted ">
                        <h1>Create New Campaign</h1>
                        <hr>
                    </div>
                    <form role="form"  method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
               <div class="row">
               	
                        <div class="form-group col-md-12 ">
                            <label for="title" class="col-md-12 control-label">Title</label>
                            
                            <div class="input-group input-group-alternative ">
                                
                              
                                <input class="form-control" placeholder="Title" type="text" name="title"  id="title"  value="{{ old('title') }}" required autofocus>
                           
                            </div>
                        </div>
               </div>
               <div class="row">
               	
                        <div class="form-group col-md-12">
                            <label for="title" class="col-md-12 control-label">Description</label>

                            <div class="input-group input-group-alternative ">
                                       <textarea class="form-control" name="description" id="description" cols="30" rows="2" placeholder="" >{{ old('description')}}</textarea>
                                {{-- <input class="form-control" placeholder="Description" type="text" name="description" id="description" value="{{ old('description') }}" required> --}}
                               
                            </div>
                        </div>
               </div>

               <div class="row">

                       <div class="form-group col-md-4">
                            <label for="title" class="col-md-12 control-label">Country</label>

                            <div class="input-group input-group-alternative">
                             
                                <input class="form-control" placeholder="Country" type="text" id="country" name="country" required>
                           
                            </div>
                            
                        </div>
               	
                        <div class="form-group col-md-4">
                            <label for="title" class="col-md-12 control-label">City</label>

                            <div class="input-group input-group-alternative">
                              
                                <input class="form-control" placeholder="City" type="password" name="city" id="city" required>
                           
                            </div>
                            
                        </div>
                        <div class="form-group col-md-4">
                            <label for="title" class="col-md-12 control-label">State</label>

                            <div class="input-group input-group-alternative">
                              
                                <input type="text" class="form-control" id="state" name="state" placeholder="State" required>
                            </div>
                        </div>

                        
                  
               </div> 

                 <div class="row">
               	
                        <div class="form-group col-md-6">
                            <label for="title" class="col-md-12 control-label">Start Date</label>

                            <div class="input-group input-group-alternative">
                              
                                <input class="form-control" placeholder="Start Date" type="text" name="startDate" id="startDate" required>
                           
                            </div>
                            
                        </div>
                        <div class="form-group col-md-6">
                            <label for="title" class="col-md-12 control-label">End Date</label>

                            <div class="input-group input-group-alternative">
                              
                                <input type="text" class="form-control" id="endDate" name="endDate" placeholder="End Date" required>
                            </div>
                        </div>

                     
                  
               </div>
                        <div class="">
                            <button type="submit" class="btn btn-success mt-4">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>

{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" ></script> --}}
{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script> --}}
<script src="//cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
<script>

// $( 'description' ).ckeditor();
                CKEDITOR.replace( 'description' );

</script>

<script type="text/javascript">
  $( function() {
   $( "#startDate" ).datepicker();
// });
 
//   $( function() {
   $( "#endDate" ).datepicker();
});

</script>
@endsection
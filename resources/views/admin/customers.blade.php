@extends('layouts.app')
@section('content')
<div class="header pt-md-8">
	<div class="container-fluid">
		<div class="header-body">
			
			<div class="row mt-5">
				<div class="col">
			<div class="flash-message">
						@foreach (['danger', 'warning', 'success', 'info'] as $msg)
						@if(Session::has('alert-' . $msg))
						<p class="alert alert-"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
						@endif
						@endforeach
						
					</div>
					<div class="card bg-default shadow">
						<div class="card-header bg-transparent border-0">
							{{-- <h3 class="text-white mb-0">Customers</h3> --}}
							<div class="row">
								<div class="col-lg-6">
									
									<h3 class="text-white mb-0">Customers Information</h3>
								</div>
								<div class="col-lg-6">
									<button type="button" class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#customerinfo_import">
										Import
									</button>
									<button type="button" class="btn btn-primary btn-sm float-right mr-2" data-toggle="modal" data-target="#customerinfo_form">
										Add New
									</button>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table align-items-center  hover cell-border table-light table-flush" id="myTable" >
								<thead class="thead-dark">

									<tr class="topHead">
										<th scope="col">ID<br><input class="form-control searchFilter"  type="text" ></th>
										<th scope="col">REGISTRANT NAME<br><input class="form-control searchFilter" data-order="1" type="text" ></th>
										<th scope="col">REGISTRANT ADDRESS<br><input class="form-control searchFilter" data-order="2" type="text" ></th>
										<th scope="col">REGISTRANT PHONE<br><input class="form-control searchFilter" data-order="3" type="text" ></th>
										<th scope="col">REGISTRANT EMAIL<br><input class="form-control searchFilter" data-order="4" type="text" ></th>
										<th scope="col">REGISTRANT ZIP<br><input class="form-control searchFilter" data-order="5" type="text" ></th>
										<th scope="col">REGISTRANT COUNTRY<br><input class="form-control searchFilter" data-order="6" type="text" ></th>
										<th scope="col">REGISTRANT STATE<br><input class="form-control searchFilter" data-order="7" type="text" ></th>
										<th scope="col">REGISTRANT CITY<br><input class="form-control searchFilter" data-order="8" type="text" ></th>
										<th scope="col">STATUS<br><input class="form-control searchFilter" data-order="9" type="text" ></th>
										<th scope="col">ACTION<br>
											<select class=""  data-order="10" name="customer_select" id="customer_select">
												<option value="" selected hidden></option>
												<option value="1">Active All</option>
												<option value="0">Inactive All</option></select></th>
										{{-- <th scope="col"></th> --}}
									</tr>
								</thead>
								<tbody>
									@foreach($customers as $customer)
									<tr>
										<th>{{$customer->id }}</th>
										<td>{{$customer->registrant_name }}</td>
										<td>{{$customer->registrant_address }}</td>
										<td>{{$customer->registrant_phone}}</td>
										<td>{{$customer->registrant_email}}</td>
										<td>{{$customer->registrant_zip}}</td>
										<td>{{$customer->country->name}}</td>
										<td>{{$customer->state->name}}</td>
										<td>{{$customer->city->name}}</td>
										{{-- <td>{{$customer->state}}</td> --}}
										<td>
											@if($customer->status =='0')
											<span class="text-danger">Inactive
											</span>
											@else
											<span class="text-success" >Active</span>
											@endif
										</td>
										<td class="">
											@if($customer->status=='0')
											<a onclick="status_update('{{$customer->id }}');" ><svg width="25px" hieght="25px" aria-hidden="true" focusable="false" data-prefix="fad" data-icon="toggle-on" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-toggle-on fa-w-18 fa-3x"><g class="fa-group"><path fill="#ffffff" d="M384 384a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-secondary"></path><path fill="#ff0000" d="M384 64H192C86 64 0 150 0 256s86 192 192 192h192c106 0 192-86 192-192S490 64 384 64zm0 320a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-primary"></path></g></svg></a>
											@elseif($customer->status=='1')<a onclick="status_update('{{$customer->id }}');" ><svg width="25px" hieght="25px" aria-hidden="true" focusable="false" data-prefix="fad" data-icon="toggle-off" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-toggle-off fa-w-18 fa-3x"><g class="fa-group"><path fill="#44c70e" d="M384 64H192C86 64 0 150 0 256s86 192 192 192h192c106 0 192-86 192-192S490 64 384 64zM192 384a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-secondary"></path><path fill="#ffffff" d="M192 384a128 128 0 1 1 128-128 127.93 127.93 0 0 1-128 128z" class="fa-primary"></path></g></svg></a>
											@endif
											{{-- <button
												class="btn btn-link"
												data-toggle="modal"
												data-target="#customerinfo_form"
												data-name="{{ $customer->registrant_name }}"
												data-email="{{ $customer->registrant_email }}"
												data-phone="{{ $customer->registrant_phone }}"
												data-address="{{ $customer->registrant_address }}"
												data-zip="{{ $customer->registrant_zip }}"
												data-country="{{ $customer->country->id }}"
												data-state="{{ $customer->state->id }}"
												data-city="{{ $customer->city->id }}"
											> --}}
											<a  onclick="open_modal({{$customer->id}})">
												<svg aria-hidden="true" width="20px" height="20px" focusable="false" data-prefix="fas" data-icon="edit" class="svg-inline--fa fa-edit fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z"></path></svg>
											</a>
											<!-- </button> -->
										</td>
										
									</tr>
									@endforeach
									
								</tbody>
								<tfoot>
									
								</tfoot>
							</table>

						</div>
					</div>
				</div>
			</div>
			<form id="modelForm">
				{{ csrf_field() }}
				{{-- {{ method_field('PUT') }} --}}

				
				<!-- Modal -->
				<div class="modal " id="customerinfo_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h2 class="modal-title" id="exampleModalLongTitle">Customer Information</h2>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<div class="card bg-default shadow">
									
									
									<div class="container">
										
									{{-- 	@php 
										echo '<pre>';
										print_r($country);
										echo '<pre>';

										die();
										@endphp --}}
									{{-- 	@if ($errors->has('name') || $errors->has('email') || $errors->has('phone') || $errors->has('city')|| $errors->has('country') || $errors->has('state'))
										<div class="has-error">
											<span class="help-block">
												<strong>{{ $errors->first('name') }}</strong>
												<strong>{{ $errors->first('email') }}</strong>
												<strong>{{ $errors->first('phone') }}</strong>
												<strong>{{ $errors->first('city') }}</strong>
												<strong>{{ $errors->first('country') }}</strong>
												<strong>{{ $errors->first('state') }}</strong>
											</span>
										</div>
										@endif --}}
										<input type="text" id="id" value="" name="id" hidden>
										
										<hr class="my-4" />
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<label class="form-control-label" for="registrant_name"><span class="text-white">Registrant Name</span></label>
													<input type="text" id="registrant_name" value="" name="registrant_name" class="form-control form-control-alternative" placeholder="Jesse">
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<label class="form-control-label" for="registrant_email"><span class="text-white">Registrant Email</span></label>
													<input type="email" id="registrant_email" value="" name="registrant_email" class="form-control form-control-alternative" placeholder="jesse@example.com">
													<span id="error_registrant_email"></span>
												
												</div>
											</div>
											
										{{-- </div> --}}
											<div class="col-lg-4">
												<div class="form-group">
													<label class="form-control-label" for="registrant_phone"><span class="text-white">Registrant Phone</span></label>
													<input type="tel" id="registrant_phone" value="" name="registrant_phone" class="form-control form-control-alternative" placeholder="+16153921187">
												</div>
											</div>
											</div>
											
										{{-- <hr class="my-4" /> --}}
											
										<div class="row">
											
											<div class="col-lg-8">
												<div class="form-group">
													<label class="form-control-label" for="registrant_address"><span class="text-white">Registrant Address</span></label>
													<input type="text" id="registrant_address" value="" name="registrant_address" class="form-control form-control-alternative" placeholder="181 St. 11th Township">
												</div>
											</div>
										{{-- </div> --}}
										{{-- <hr class="my-4" />
										<div class="row"> --}}
											<div class="col-lg-4">
												<div class="form-group">
													<label class="form-control-label" for="registrant_zip"><span class="text-white">Registrant Zip</span></label>
													<input type="text" id="registrant_zip" value="" name="registrant_zip" class="form-control form-control-alternative" placeholder="LA72091" >
												</div>
											</div>
											</div>

										 {{-- <hr class="my-4" /> --}}

										<div class="row">

											<div class="col-lg-4">
												<div class="form-group">
													<label class="form-control-label" for="registrant_country"><span class="text-white">Registrant Country</span></label>

													<select class="form-control form-control-alternative" id="registrant_country" onchange="getstate()" name="registrant_country" >
														<option  value=""selected hidden>Select Country</option>
														@foreach ($countries as $country)
														{{-- <option  value="" selected="selected" ></option> --}}
														<option  value="{{$country->id}}" >{{$country->name}}</option>
														@endforeach
													</select>
													{{-- <label class="form-control-label" for="registrant_country"><span class="text-white">Registrant Country</span></label>
													<input type="text" id="registrant_country" value="" name="registrant_country" class="form-control form-control-alternative" placeholder="Washington, DC"> --}}
												</div>
											</div>

											<div class="col-lg-4">
												<div class="form-group">
													<label class="form-control-label" for="registrant_state"><span class="text-white">Registrant State</span></label>

													<select class="form-control form-control-alternative" id="registrant_state" onchange="getcity()" name="registrant_state" disabled >
														<option  value=""  hidden>Select State</option>
														{{-- @foreach ($countries as $country) --}}
														{{-- <option  value="" selected="selected" ></option> --}}
														{{-- <option  value="{{$country->name}}" >{{$country->name}}</option> --}}
														{{-- @endforeach --}}
													</select>
													{{-- <input type="text" id="registrant_state" value="" name="registrant_state" class="form-control form-control-alternative" placeholder="United States" > --}}
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<label class="form-control-label" for="registrant_city"><span class="text-white">Registrant City</span></label>

													<select class="form-control form-control-alternative"  id="registrant_city" name="registrant_city" disabled >
														<option  value=""  hidden>Select City</option>
														{{-- @foreach ($countries as $country) --}}
														{{-- <option  value="" selected="selected" ></option> --}}
														{{-- <option  value="{{$country->name}}" >{{$country->name}}</option> --}}
														{{-- @endforeach --}}
													</select>

													{{-- <input type="text" id="registrant_city" value="" name="registrant_city" class="form-control form-control-alternative" placeholder="Washington, DC"> --}}
												</div>
											</div>
										</div>
										
										<hr class="my-4" />
										
										{{--
										<div class="row">
											<div class="col-lg-12">
												<input type="submit" class="btn btn-success" value="Save">
											</div>
										</div>
										
										<hr class="my-4" /> --}}
									</div>
								</div>
							</div>
							<div class="modal-footer">
								{{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
								{{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
								<button type="submit" class="btn btn-success" id="save">Save</button> {{-- onclick="validateForm()" --}}
							</div>
						</div>
					</div>
				</div>
			</form>
			<form id="customerinfo_importModal" enctype="multipart/form-data" >
				{{ csrf_field() }}
				<!-- Modal -->
				<div class="modal " id="customerinfo_import" tabindex="-1" role="dialog" aria-labelledby="customerinfo_importTitle" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h2 class="modal-title" id="customerinfo_importTitle">Import Excel File</h2>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
						{{-- 	@php
								echo $errors
								@endphp --}}
								<div class="form-group">

									<div class="image-upload">
										<label for="file_input">
											<img src="./assets/img/icons/upload.png"/>
										</label>
										<p class="file-name"></p>
										<input id="file_input" name="file_input" type="file" />
										<span id="error_file_input"></span>

									</div>
								</div>
								<div class="image-loader" style="position: relative;text-align: center;">

									<img src="./assets/img/loader.gif" style="width:50%; display: none;" class="loaderGIF"  alt="loader">
								</div>


							</div>
							<div class="modal-footer">
								{{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
								{{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
								<button type="submit" class="btn btn-success" id="import">Import</button> {{-- onclick="validateForm()" --}}
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" ></script> --}}
{{-- <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script> --}}

<script type="text/javascript">

$(document).ready( function () {
	var dtable =  $('#myTable').DataTable({
		sDom: 'lrtip',
		"bSort" : false,
		"pagingType": "full_numbers",
		"processing": true,
		"scrollX": true,
		"autoWidth": false,
		
	});

	$('.searchFilter').on('keyup', function() {
		if (this.value.length) {
			dtable.column($(this).attr('data-order')).search(this.value, true);
			dtable.draw();
		}
		if (!$(this).val()) {
			dtable.column($(this).attr('data-order')).search(this.value, true);
			dtable.draw();
		}
	});



	$('input[type="file"]').change(function() {
		if ($(this).val()) {
			var filename = $(this).get(0).files.item(0).name;
			$(this).closest('.image-upload').find('.file-name').html(filename);
		}
	});



	$("#customer_select").change(function () {
		var current_ids = [];
		// console.log($("#customer_select").val());
		// alert();
 
    // dtable.column($(this).attr('data-order')).data()
    //    .each( function ( value, index ) {
    //    
       // $('button').on('click', function(){

    var confirmation = confirm("Are you sure you want to change status?");

   

       	var current = dtable.rows( { page: 'current' } ).data().each(function(index, el) {
       		current_ids.push(index[0])
       		// var array = index[0];
       		
       	// console.log(el);
       	});
       	// var url = ""
       			// console.log(current_ids);
       	
       				 if (confirmation) {
       	$.ajax({
       		url: '{{ route('customers.update_customers_status')}}',
       		type:'post',
       		data: {
				_token: '{{csrf_token()}}',
				IDs :	current_ids,
				option : $("#customer_select").val(),
			},
       		success: function(data){
       			console.log(data);
       			if (data.status != 0) {
       				location.reload();

       			}
       		
    
    // 	else{
				// $("#customer_select option[value='']").prop('selected', true);
       			}
       			// console.log(data);
       		
       		

       	});
       }
    	else{
				$("#customer_select option[value='']").prop('selected', true);
       			}
       
       	// console.log(current_ids);
       	// console.log(dtable.page.info());
       	// console.log(dtable.rows( { page: 'current' } ).data());
           // console.log( 'Data in index: '+index+' is: '+value );
 
           // dtable.column($(this)).cells().invalidate().render();
       // } );
    


       } );
});



// $(document).ready(function() {
// 	getstate();
// getcity();

// });

function getstate()
{
	var country = $('#registrant_country').val();
	var states='';
	var url = "{{ route('customers.get_states')}}";  
	$.ajax({
		type: "post",
		dataType: 'json',
		url: url,
		data: {
			_token: '{{ csrf_token() }}',
			countryID: country,
		},
		success: function(data) {
			if(data.status==1){
				$.each(data.data,function(key, value) {
					// console.log(value.name);
					states+='<option value="'+value.id+'">'+value.name+'</option>';
					$('#registrant_state').html(states);
				});
				$("#registrant_state").prop('disabled', false);

			}          
		},
		error: function(data) {
			console.log('error');
		}
	});
}


function getcity()
{
	// alert();
	var state = $('#registrant_state').val();
	// console.log(state);
	var cities='';
	var url = "{{ route('customers.get_cities')}}";  
	$.ajax({
		type: "post",
		dataType: 'json',
		url: url,
		data: {
			_token: '{{ csrf_token() }}',
			stateID: state,
		},
		success: function(data) {
			// alert(state);
			if(data.status==1){
				$.each(data.data,function(key, value) {
					cities+='<option value="'+value.id+'">'+value.name+'</option>';
					$('#registrant_city').html(cities);
				});
				$("#registrant_city").prop('disabled', false);

			}          
		},
		error: function(data) {
			// console.log(state);

			console.log('error');
		}
	});
}


        // $("#registrant_country").change(function(){
        //     getstate();
        // });

        // $("#registrant_country").focus(function(){
        //     getstate();
        // });

        // $("#registrant_state").change(function(){
        //     getcity();
        // });

        // $("#registrant_state").focus(function(){
        //     getcity();
        // });

	 //    $('#customerinfo_form').on('show.bs.modal', function (event) {
		//   var button = $(event.relatedTarget)
		//   var name = button.data('name')
		//   var email = button.data('email')
		//   var phone = button.data('phone')
		//   var address = button.data('address')
		//   var zip = button.data('zip')
		//   var country = button.data('country')
		//   var state = button.data('state')
		//   var city = button.data('city')
		  
		//   var modal = $(this)
		//   modal.find('#registrant_name').val(name)
		//   modal.find('#registrant_email').val(email)
		//   modal.find('#registrant_phone').val(phone)
		//   modal.find('#registrant_address').val(address)
		//   modal.find('#registrant_zip').val(zip)
		//   modal.find("#registrant_country option[value="+country+"]").attr('selected', 'selected')
		//   getstate()
		//   modal.find("#registrant_state option[value="+state+"]").attr('selected', 'selected')
		//   console.log(state)
		//   getcity()
		//   modal.find("#registrant_city option[value="+city+"]").attr('selected', 'selected')
		//   console.log(city)
		// })
   
// getcity();

function open_modal(customerId){

	$('#customerinfo_form').modal('show')
	$("#registrant_country").prop('disabled', false);
	$("#registrant_state").prop('disabled', false);
	$("#registrant_city").prop('disabled', false);



	$('button#save').html('Update');
	// console.log(customerId);

	var url = "{{ route('customers.edit')}}";
	$.ajax({
		type: "post",
		dataType: 'json',
		url: url,
		data: {
			_token: '{{csrf_token()}}',
			customerId: customerId
		},
		success: function(data) {
			var option = '';

			var data = data.data;
			console.log(data);
			Object.keys(data).forEach(index => {
     // console.log($('#customerinfo_form :input[name=name]').val(data[index[0]]).name);
     // console.log(data[index[0]].state.name);
     $('#customerinfo_form :input[name=id]').val(data[index[0]].id);
     $('#customerinfo_form :input[name=registrant_name]').val(data[index[0]].registrant_name);
     $('#customerinfo_form :input[name=registrant_email]').val(data[index[0]].registrant_email);
     $('#customerinfo_form :input[name=registrant_phone]').val(data[index[0]].registrant_phone);
     $('#customerinfo_form :input[name=registrant_address]').val(data[index[0]].registrant_address);
     $('#customerinfo_form :input[name=registrant_zip]').val(data[index[0]].registrant_zip);  
   // console.log(data[index[0]].country.name);
    // $('#customerinfo_form #registrant_country option:contains(' + data[index[0]].country.name + ')').prop({
    	// selected: true});
$('#customerinfo_form #registrant_country  option[value="' + data[index[0]].country.id + '"]').attr('selected', true);

    	getstate(); 

setTimeout(function(){
$('#customerinfo_form #registrant_state  option[value="' + data[index[0]].state.id + '"]').attr('selected', true);
    	getcity();   	
}, 100);


setTimeout(function(){
	// alert();
$('#customerinfo_form #registrant_city  option[value="' + data[index[0]].city.id + '"]').attr('selected', true);
}, 150);

       });
		},
		error: function(data) {
			console.log(data);
		}
	});

}


$(document).ready(function($) {
  // $("#modelForm").validate();
  // jQuery(function($) {
    //custom validation for phone number
    $.validator.addMethod(
    	"regex",
    	function(value, element, regexp) {
    		var re = new RegExp(regexp);
    		return this.optional(element) || re.test(value);
    	},
    	"Please check your input."
    	);
    $("#modelForm").validate({
    	rules: {
    		registrant_name: {
    			required: true
    		},
    		registrant_address: {
    			required: true
    		},
    		registrant_phone: {
    			required: true,
    			regex: /^\+[(]?[0-9]+[)]?[-\s\.]?[0-9]+[-\s\.]?[0-9]+$/
    		}, 

    		registrant_email: {
    			required: true,
    			email: true
    		},
    		registrant_zip: {
    			required: true
    		},  

    		registrant_country: {
    			required: true
    		},
    		registrant_state: {
    			required: true
    		},    
    		registrant_city: {
    			required: true
    		},

    	}
    });    
});


$(document).on('submit','#customerinfo_importModal',function(e){
      e.preventDefault();
      // $('#loaderWrap').show();
      // var file_input  = $('.file_input').val();
      var form_data = new FormData(this);
      var message = '';
      // console.log(form_data);
      var url = '{{ route("customers.import") }}';
      
      $.ajax({
        url : url,
        method : "post",
        data : form_data,
        processData : false,
        contentType : false,
        success : function(data){

        	 			// console.log(data);
	if (data.status==0) {
 		$.each(data.error, function(key, val) {
 			// console.log($('#error_'+key).html(val[0]));
 				$('#error_'+key).html(val[0]);
	 		});
 		}
 		else if (data.status==2) {

 		message += '<p class="alert alert-danger">'+data.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>'
        	 			$('.flash-message').append(message)
        	 			$('#customerinfo_importModal').hide();
        	 			setTimeout(function(){ location.reload(); }, 2000);
 		}
        else if (data.status==1){
        	 			// console.log(data.message);
        	 			message += '<p class="alert alert-success">'+data.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>'
        	 			$('.flash-message').append(message)
        	 			$('#customerinfo_importModal').hide();
        	 			setTimeout(function(){ location.reload(); }, 2000);

 		// location.reload();
 		}

        }
      });
    });
// });
 $(document).on('submit','#modelForm',function(e){
 	e.preventDefault();
 	// alert('sdfgkjfhdgkj');
 	var url = "{{ route('customers.new')}}";  
 	var message = "";  
 	$.ajax({
 		type: 'post',
 		dataType: 'json',
 		url: url,
 		data: $('#modelForm').serialize(),
 		success: function(data) {
 		console.log(data.message);	

 		if (data.status==0) {
 		$.each(data.error, function(key, val) {
 				$('#error_'+key).html(val[0]);
	 		});
 
 		}
 		else if (data.status==1) {

 		message += '<p class="alert alert-success">'+data.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>'
        	 			$('#modelForm').hide();
        	 			$('.flash-message').append(message)
        	 			setTimeout(function(){ location.reload(); }, 2000);
 		}
        else if (data.status==2){
        	 			// console.log(data.message);
        	 			message += '<p class="alert alert-success">'+data.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>'
        	 			$('#modelForm').hide();
        	 			$('.flash-message').append(message)
        	 			setTimeout(function(){ location.reload(); }, 2000);

 		// location.reload();
 		}
 	
		},
		error: function(data) {
 		console.log(data.message);	
		}
 	});
 });






function status_update(id)
{
	var url = "{{ route('customers.status')}}";
	$.ajax({
		type: "post",
		dataType: 'json',
		url: url,
		data: {
			_token: '{{csrf_token()}}',
			customerId: id
		},
		success: function(data) {
			// console.log(data);

			location.reload();
		},
		error: function(data) {
			console.log(data);
		}
	});
}
</script>
@endsection
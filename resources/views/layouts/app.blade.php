@include('includes.header')
@if(\Auth::check())
	@include('includes.navigation')
@endif
@yield('content')
@include('includes.footer')
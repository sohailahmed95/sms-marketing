@extends('layouts.app')
@section('content')
<div class="container pt-7">
    <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
            <div class="car bg-white shadow rounded">
                {{--        <div class="card-header bg-transparent pb-5">
                    <div class="text-muted text-center mt-2 mb-3"><small>Sign in with</small></div>
                    <div class="btn-wrapper text-center">
                        <a href="#" class="btn btn-neutral btn-icon">
                            <span class="btn-inner--icon"><img src="../assets/img/icons/common/github.svg"></span>
                            <span class="btn-inner--text">Github</span>
                        </a>
                        <a href="#" class="btn btn-neutral btn-icon">
                            <span class="btn-inner--icon"><img src="../assets/img/icons/common/google.svg"></span>
                            <span class="btn-inner--text">Google</span>
                        </a>
                    </div>
                </div> --}}
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                        @endforeach
                        
                    </div>
                <div class="card-body px-lg-5 py-lg-5">
                    
                 
                    <div class="text-center text-muted mb-4">
                        <h1>Sign In</h1>
                        <hr>
                    </div>
                    <form role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="{{ $errors->has('email') ? ' has-error' : '' || $errors->has('password') ? ' has-error' : '' }}">
                            @if ($errors->has('email') || $errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group mb-3 {{-- {{ $errors->has('email') ? ' has-error' : '' }} --}}">
                            <label for="email" class="col-md-12 control-label">E-Mail Address</label>
                            <div class="input-group ">
                              
                                <input class="form-control" placeholder="Email" type="email" name="email" value="{{ old('email') }}" required autofocus>
                                {{-- @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif --}}
                            </div>
                        </div>
                        <div class="form-group {{-- {{ $errors->has('password') ? ' has-error' : '' }} --}}">
                            <label for="password" class="col-md-12 control-label">Password</label>
                            <div class="input-group ">
                              
                                <input class="form-control" placeholder="Password" type="password" name="password" required>
                                {{--   @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif --}}
                            </div>
                        </div>
                        <div class="custom-control custom-control-alternative custom-checkbox">
                            <input class="custom-control-input" id=" customCheckLogin" name="remember" type="checkbox"{{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for=" customCheckLogin">
                                <span class="text-muted">Remember me</span>
                            </label>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary my-4">Sign in</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-6">
                    {{-- <a href="{{ route('password.request') }}" class="text-gray-dark"><small>Forgot password?</small></a>   --}}

                    {{-- <a href="{{ route('register') }}" class="text-gray-dark"><small>Register</small></a> --}}
                </div>
                {{--   <div class="col-6 text-right">
                    <a href="#" class="text-light"><small>Create new account</small></a>
                </div> --}}
            </div>
        </div>
    </div>
</div>
@endsection

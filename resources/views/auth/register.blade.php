@extends('layouts.app')
@section('content')
<div class="container pt-8">
    <!-- Table -->
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8">
            <div class="card bg-white shadow rounded">
                {{--     <div class="card-header bg-transparent pb-5">
                    <div class="text-muted text-center mt-2 mb-4"><small>Sign up with</small></div>
                    <div class="text-center">
                        <a href="#" class="btn btn-neutral btn-icon mr-4">
                            <span class="btn-inner--icon"><img src="../assets/img/icons/common/github.svg"></span>
                            <span class="btn-inner--text">Github</span>
                        </a>
                        <a href="#" class="btn btn-neutral btn-icon">
                            <span class="btn-inner--icon"><img src="../assets/img/icons/common/google.svg"></span>
                            <span class="btn-inner--text">Google</span>
                        </a>
                    </div>
                </div> --}}
                <div class="card-body px-lg-5 py-lg-5">
                    <div class="text-center text-muted mb-4">
                        <h1>Add New User</h1>
                        <hr>
                    </div>
                    <form role="form"  method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="{{ $errors->has('name') || $errors->has('email') || $errors->has('password')  ? ' has-error' : '' }}">
                            @if ($errors->has('name') || $errors->has('email') || $errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                                <strong>{{ $errors->first('email') }}</strong>
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group ">
                            <div class="input-group input-group-alternative mb-3">
                              
                                <input class="form-control" placeholder="Name" type="text" name="name" value="{{ old('name') }}" required autofocus>
                                {{--        @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif --}}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group input-group-alternative mb-3">
                              
                                <input class="form-control" placeholder="Email" type="email" name="email" value="{{ old('email') }}" required>
                                {{--
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif --}}
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="input-group input-group-alternative">
                             
                                <input class="form-control" placeholder="Password" type="password" name="password" required>
                                {{--   @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif --}}
                            </div>
                            
                        </div>
                        <div class="form-group ">
                            <div class="input-group input-group-alternative">
                                
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password"required>
                            </div>
                        </div>
                        {{-- <div class="text-muted font-italic"><small>password strength: <span class="text-success font-weight-700">strong</span></small></div> --}}
                        {{--      <div class="row my-4">
                            <div class="col-12">
                                <div class="custom-control custom-control-alternative custom-checkbox">
                                    <input class="custom-control-input" id="customCheckRegister" type="checkbox">
                                    <label class="custom-control-label" for="customCheckRegister">
                                        <span class="text-muted">I agree with the <a href="#!">Privacy Policy</a></span>
                                    </label>
                                </div>
                            </div>
                        </div> --}}
                        <div class="text-center">
                            <button type="submit" class="btn btn-success mt-4">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/checkenv',function(){
	$test = 'test api';
   putenv ("TWILIO_SID=".$test);
   echo getenv('TWILIO_SID');
   die;
});

Route::get('/', function () {
    return view('auth.login');
});

// Authentication Routes...
// 
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
// 
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
// 
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');



Route::get('/home', 'HomeController@index')->name('home');


Route::namespace('Admin')->group(function () {
	Route::get('/users', 'Users@users_list')->name('user.user_list');
	Route::post('/user_status', 'Users@status_update')->name('user.status_update');
	Route::get('/customers', 'CustomerController@show')->name('customers.list');
	Route::post('/new_customer', 'CustomerController@create')->name('customers.new');
	Route::post('/customer_status', 'CustomerController@status_update')->name('customers.status');
	Route::post('/update_customers_status', 'CustomerController@update_customers_status')->name('customers.update_customers_status');
	Route::post('/import_record', 'CustomerController@import')->name('customers.import');
	Route::post('/edit_customer', 'CustomerController@edit')->name('customers.edit');



    /*-----------------Campaign Routes-----------------------------------------*/
	Route::get('/campaigns', 'CampaignController@index')->name('campaigns.index');
	Route::get('/campaigns/create', 'CampaignController@create')->name('campaigns.create');
	Route::post('/campaigns/store', 'CampaignController@store')->name('campaigns.store');
	Route::get('/campaigns/edit/{id}', 'CampaignController@edit')->name('campaigns.edit');
	Route::post('/campaigns/update', 'CampaignController@update')->name('campaigns.update');
	Route::post('/campaigns/status', 'CampaignController@status')->name('campaigns.status');
	Route::get('campaigns/sms/{id}','CampaignController@smsview')->name('campaigns.smsview');
	Route::post('/campaign_status', 'CampaignController@status_update')->name('campaigns.status');
	Route::post('/campaign_message', 'CampaignController@get_message')->name('campaigns.message');
	Route::post('/show_message', 'CampaignController@show_message')->name('campaigns.show_message');

	/*-----------------Campaign Routes End-----------------------------------------*/

	/*-------------------------Send SMS Route--------------------------------------*/
    Route::get('/sms', 'SMSController@index')->name('sms.index');
    Route::post('/sendsms','SMSController@sendsms')->name('sms.send');
	/*-------------------------Send SMS Route End--------------------------------------*/
	

	/*-------------------------Settings Route--------------------------------------*/
    Route::get('/settings', 'SettingController@index')->name('settings.index');
    Route::get('/settings_new', 'SettingController@store')->name('settings.store');
    Route::post('/settings_save', 'SettingController@update')->name('settings.update');
    Route::post('/remove_api/{id}', 'SettingController@destroy')->name('settings.destroy');
    Route::get('/change_api', 'SettingController@change_api')->name('settings.change_api');

	/*-------------------------Settings Route End--------------------------------------*/
   

    /*-----------------Country State City Routes-----------------------------------------*/
	Route::get('/getstates/{id}', 'CampaignController@getstates')->name('campaigns.getstates');
	Route::get('/getcities/{id}', 'CampaignController@getcities')->name('campaigns.getcities');
	
	Route::post('/getstates', 'CustomerController@get_states')->name('customers.get_states');
	Route::post('/getcities', 'CustomerController@get_cities')->name('customers.get_cities');
	/*-----------------Country State City Routes End-----------------------------------------*/

});


// Route::get('test', function(){
// 	echo "testing";
// });
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class import extends Model
{
    protected $table = "imports";
    protected $fillable = ['file_title','total_rows','saved_rows','empty_rows','created_at','updated_at'];

}

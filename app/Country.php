<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = "country";
    protected $fillable = ['name'];

     public function state(){
    	return $this->hasMany('App\State');
    }

      public function customer(){
    	return $this->belongsTo('App\Customer');
    }

    

}

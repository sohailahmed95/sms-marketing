<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = "state";
    protected $fillable = ['name','country_id'];

    public function country(){
    	return $this->belongsTo('App\Country');
    }


     public function city(){
    return	$this->hasMany('App\City');
    }

    
      public function customer(){
    	return $this->belongsTo('App\Customer');
    }
}

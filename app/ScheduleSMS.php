<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleSMS extends Model
{
    protected $table = "schedule_sms";
    protected $fillable = ['campaign_id','customer_id','sms_status_id','sms_exception'];

    public function campaign()
    {
    	return $this->hasone('App\Campaign','id','campaign_id');
    }

    public function campaign_result()
    {
        return $this->hasone('App\ScheduleSMS','id','campaign_id');
    }

    public function customer()
    {
    	return $this->hasone('App\Customer','id','customer_id');
    }

    // public function status()
    // {
    // 	return $this->hasone('App\SMSStatus','id','sms_status');
    // }
    // 
    
     public function sms_status()
    {
        return $this->belongsTo('App\MessageStatus');
    }

}



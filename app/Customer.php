<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = "customers";
      protected $fillable = ['registrant_name','registrant_phone','registrant_address','registrant_zip','registrant_email','registrant_country_id','registrant_state_id','registrant_city_id','status'];

        public function country()
    {
        return $this->hasOne('App\Country','id','registrant_country_id');
    }


      public function state()
    {
        return $this->hasOne('App\State','id','registrant_state_id');
    }
      public function city()
    {
        return $this->hasOne('App\City','id','registrant_city_id');
    }

    
}

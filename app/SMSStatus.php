<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SMSStatus extends Model
{
    protected $table = "message_status";
    protected $fillable = ['name'];
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageStatus extends Model
{
     protected $table = "sms_status";
     protected $fillable = ['id','status'];

        public function status(){
        return $this->hasMany('App\ScheduleSMS');
    }

}
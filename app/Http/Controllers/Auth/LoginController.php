<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/campaigns';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

        public function logout()
    {
        $this->guard()->logout();

        return redirect()->route('login');
    }

    public function login(Request $request)
{
    $credentials = $request->only('email', 'password','is_active');
    $remember = (Input::has('remember')) ? true : false;

     if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $remember)) {
        if(Auth::check() && Auth::user()->is_active ==0){
             Auth::logout();
             $request->session()->flash('alert-danger', $request->email.' is not approved yet!');        
             return redirect()->route('login'); 
        }
        else{
            return redirect()->intended('campaigns');
        }    
    }
    else{
         $request->session()->flash('alert-danger', $request->email.' is not registered');        
         return redirect()->route('login'); 

    }
}

}

<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\City;
use App\Country;
use App\Customer;
use App\Campaign;


class ApiController extends Controller
{
    public function campaign_sms(Request $request)
    {
        dd($request->all());
        try
        {
            $rules = [
                'campaign_id'=>'required|exists:campaign,id',
                'message'=>'required'
            ];

            $validator = Validator::make($request->all(),$rules);
            if($validator->fails()){
                return response()->json([
                    'status'=>'Error',
                    'message'=>$validator->errors()
                ],422);
            }

            $campaign=Campaign::where('id',$request->campaign_id)->first();

            $users=Customer::where('registrant_country_id',$campaign->country)
            ->where('registrant_state_id',$campaign->state)
            ->where('registrant_city_id',$campaign->city)
            ->select('phone')
            ->get();

            //dd($users);
            foreach($users as $user)
            {
                $response = sendMessage($request->message,$user->phone);
                //dd($response);
            }

            return response()->json([
                    'status'=>'Success',
                    'message'=>'Not data found'
                ],200);

        }
        catch (Exception $exception) {
            return response()->json([
                'status' => 'Error',
                'message' => $exception->getMessage()
            ], 200);
        }  
    }
}

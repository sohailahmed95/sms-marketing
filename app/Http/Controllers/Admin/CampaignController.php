<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Campaign;
use App\Country;
use App\State;
use App\City;
use App\ScheduleSMS;
use Illuminate\Http\Request;
use Session;
use DB;

class CampaignController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $campaigns=Campaign::with(['getcountry','getstate','getcity'])->get();
       foreach ($campaigns as $value) {
       if (($value->start< date("Y-m-d H:i:s")) || ($value->end < date("Y-m-d H:i:s"))) {
        DB::table('campaign')->where('id',$value->id)->update(['status' => 0]);
       } 
       else{
       }
       
       }
       // dd($campaigns[0]->start);
       return view('admin.campaigns.index',compact(['campaigns']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries=Country::all();
        return view('admin.campaigns.create',compact(['countries']));
    }

    public function getstates($id)
    {
       $states = State::where('country_id',$id)->get();
       
       return response()->json([
        'status' => true, 
        'states' => $states
       ], 200 );
    }

    public function getcities($id)
    {
        $cities = City::where('state_id',$id)->get();
       
       return response()->json([
        'status' => true, 
        'cities' => $cities
       ], 200 );
    }
  
   public function get_message(Request $request)
    {
        $message = Campaign::where('id',$request->campaignId)->first();
       
       return response()->json([
        'status' => 1, 
        'data' => $message
       ], 200 );
    }
 
   public function show_message(Request $request)
    {
        $message = Campaign::find($request->campaignID);
       
       return response()->json([
        'status' => 1, 
        'data' => $message
       ], 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $campaign = new Campaign;
        $startTime = Carbon::parse($request->start)->format('Y-m-d H:i:s');
        $endTime = Carbon::parse($request->end)->format('Y-m-d H:i:s');
        // dd($request->start);

        $campaign->title = $request->title;
        $campaign->description = $request->description;
        $campaign->country_id = $request->country;
        $campaign->state_id = $request->state;
        $campaign->city_id = $request->city;
        $campaign->start = $startTime;
        $campaign->end = $endTime;
        // dd($request->all());
        // $campaign->fill($request->all());

        if($campaign->save()){
            /*$request->session()->flash('alert-success', 'Customer Info Updated! - ID: '.$request->id);        
            return redirect()->back();*/
            Session::flash('alert-success','Campaign Succesfully Added');
            return redirect('campaigns');
        }
        else
        {
            Session::flash('alert-success','Error while saving Campaign');
            return redirect('campaigns');
        }
    }

    public function smsview($id)
    {
       $campaign_sms=ScheduleSMS::with(['campaign','customer'])->where('campaign_id',$id)->get();
       // dd($campaign_sms[0]->campaign->sms_status[0]->sms_exception);
       // dd($campaign_sms->campaign->description);
       return view('admin.campaigns.sms',compact('campaign_sms'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function show(Campaign $campaign)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        $campaigns = Campaign::with(['getcountry','getcity','getstate'])->where('id',$id)->first();
        // dd($campaigns);

        $countries=Country::all();
        $states=State::all();
        $cities=City::all();
        return view('admin.campaigns.edit',compact(['campaigns','countries','states','cities']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->all());
        /*$rules = [
            'title'=>'required',
            'description'=>'required',
            'country'=>'required',
            'city'=>'required',
            'state'=>'required',
            'start_date'=>'required',
            'end_date'=>'required'
        ];

        $validator = \Validator::make($request->all(),$rules);
        if($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }*/


        $campaign = Campaign::find($request->id);
        $campaign->fill($request->all());

        if($campaign->update())
        {
            Session::flash('alert-success','Campaign successfully updated');
            return redirect('campaigns');
        }
        else
        {
            Session::flash('alert-danger','Error while updating Campaign');
            return redirect('campaigns');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function destroy(Campaign $campaign)
    {
        $campaigns = Campaign::where( 'id', $request->id )->first();
        if($campaigns->delete()){
            Session::flash('alert-success','Campaign successfully deleted');
            return redirect('campaigns');
        }
        else{
            Session::flash('alert-danger','Error while deleting Campaign');
            return redirect('campaigns');
        }
    }

    public function status_update(Request $request)
    {
        $campaign = Campaign::where('id',$request->id)->first();
        if ($campaign->status  == 0) 
        {
          $campaign->status = 1;
          $campaign->save();
        } 
        elseif ($campaign->status  == 1) 
        {
          $campaign->status = 0;
          $campaign->save();
        }

        return response()->json(['status'=>true]); 
    }
}

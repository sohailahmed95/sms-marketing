<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Customer;
use App\Country;
use App\State;
use App\City;
use Excel;
use DB;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    // {    $data = Customer::state()->get();
    {  
        // dd($request->registrant_name);


//      $data = new Customer();
// // $data->state();
// echo '<pre>';
//         var_dump($data->state()->get());
// echo '<pre>';
        
//         die();
        $edit_customer = Customer::find($request->id);

        if (isset($edit_customer->id)) {
            if($edit_customer->email != $request->email){

             $rules = [
            'registrant_email' => 'unique:customers,registrant_email',
          
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
        // return redirect()->back()->with('errors',$validator->errors());      
        return response()->json(['status'=>0,'error'=>$validator->errors()]);
               
             }

        }
        $customer =  Customer::find($edit_customer->id);
        $customer->registrant_name = $request->registrant_name;
        $customer->registrant_email = $request->registrant_email;
        $customer->registrant_phone = $request->registrant_phone;
        $customer->registrant_address = $request->registrant_address;
        $customer->registrant_zip = $request->registrant_zip;
        $customer->registrant_country_id = $request->registrant_country;
        $customer->registrant_state_id = $request->registrant_state;
        $customer->registrant_city_id = $request->registrant_city;
        $customer->save();
        // $request->session()->flash('alert-success', 'Customer Info Updated! - ID: '.$request->id);        
        // return redirect()->back(); 
        return response()->json(['status'=>1,'data'=>$customer,'message'=>'Customer Info Updated! - ID: '.$request->id]);

        
        } 

        else {
          $rules = [
            'registrant_name' => 'required',
            'registrant_email' => 'required|unique:customers,registrant_email',
            'registrant_phone' => 'required',
            'registrant_address' => 'required',
            'registrant_zip' => 'required',
            'registrant_country' => 'required',
            'registrant_state' => 'required',
            'registrant_city' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
        // dd($validator->getMessageBag());

        // return redirect()->back()->with('errors',$validator->errors())->withInput(); 
        return response()->json(['status'=>0,'error'=>$validator->errors() ]);
        // return response()->json(['status'=>0, 'error'=>$validator->errors()]);

              
        }
        $customer = new Customer();
        $customer->registrant_name = $request->registrant_name;
        $customer->registrant_email = $request->registrant_email;
        $customer->registrant_phone = $request->registrant_phone;
        $customer->registrant_address = $request->registrant_address;
        $customer->registrant_zip = $request->registrant_zip;
        $customer->registrant_country_id = $request->registrant_country;
        $customer->registrant_state_id = $request->registrant_state;
        $customer->registrant_city_id = $request->registrant_city;
        $customer->save();
        // $request->session()->flash('alert-success', 'New Customer Added!');        
        // return redirect()->back();
        return response()->json(['status'=>2,'data'=>$customer,'message'=>'New Customer Added!']);

   
        
        // return response()->json(['status'=>true,'errors'=>$validator->errors()]);
}
    
    }

      public function status_update(Request $request)
    {   
     // die();
      $customer = Customer::where('id',$request->customerId)->first();
        if ($customer->status  == 0) {
        
          $customer->status = 1;
          $customer->save();
        } 
        elseif ($customer->status  == 1) {
        
          $customer->status = 0;
          $customer->save();
        } 
      
        return response()->json(['status'=>true,'data'=>$customer]);
    }  


    public function update_customers_status(Request $request)
    {   

     foreach ($request->IDs as $id) {
      $customer = Customer::find($id);
        if ($request->option == 1  && $customer->status  == 0) {
        
          $customer->status = 1;
          $customer->save();
        } 
        elseif ($request->option == 0 && $customer->status  == 1) {
        
          $customer->status = 0;
          $customer->save();
        } 
        else{
        }
     }
      
        return response()->json(['status'=>1,'c_status'=>$customer->status]);
    }

    public function import(Request $request)
    {

        ini_set('max_execution_time', 20000);

        $rules = [
            'file_input' => 'required|mimetypes:application/octet-stream,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ]; 

        $errors = [
            'file_input.required' => 'Please select a file',
            'file_input.mimetypes' => 'Select only XLS/XLSX file',
          
        ];

        $validator = Validator::make($request->all(), $rules, $errors);

        if ($validator->fails()) {
        return response()->json(['status'=>0,'error'=>$validator->errors()]);               
        }
        $file_name = $request->file('file_input')->getClientOriginalName();
        $path = $request->file('file_input')->getRealPath();
        
        // dd($request->file('file_input')->getClientOriginalName());
       // $data = Excel::load($path)->get();
          // $data = '';
        $data  = Excel::load($path, function($reader) {
        // $headings = array('registrant_name','registrant_address','registrant_phone','registrant_zip','registrant_email','registrant_country','registrant_state','registrant_city');

        // $reader->first()->only($headings);
         // $reader->limitRows(100);
         $reader->calculate(false);
         // dd($reader->getRowCount());

    });

         // if($data->count() > 0)
             // {
             // $data = $data->toArray();

            
            $total_rows = 0;
            $not_empty_rows = 0;
              foreach($data->toArray() as $value)
              {
            $total_rows++;

             if(isset($value['registrant_phone'])){

                $country = DB::table('country')->where('name','like',$value['registrant_country'])->first();
                $state = DB::table('state')->where('name','like',$value['registrant_state'])->first();
                $city = DB::table('city')->where('name','like',$value['registrant_city'])->first();
                $phone_number = substr( $value['registrant_phone'], 0, 1) == '+'? $value['registrant_phone'] : '+'.$value['registrant_phone'];
                $registrant_email = '';
                $rules = [
                'registrant_email' => 'unique:customers,registrant_email',
                 ];
                    $validator = Validator::make($value, $rules);

                    if ($validator->fails()) {
                        $registrant_email = 'N/A';               
                  }
                  else{
                        $registrant_email = $value['registrant_email'];
                  }

                $insert[] = array(
                 'registrant_name'  => $value['registrant_name']  ?? 'N/A' ,
                 'registrant_address'   => $value['registrant_address'] ?? 'N/A'  ,
                 'registrant_phone'   => $phone_number ?? 'N/A' ,
                 'registrant_zip'   => $value['registrant_zip']  ?? 'N/A' ,
                 'registrant_email'   => $registrant_email ?? 'N/A'  ,
                 'registrant_country_id'   => $country->id ?? 'N/A'  ,
                 'registrant_state_id'   => $state->id ?? 'N/A'  ,
                 'registrant_city_id'   => $city->id ?? 'N/A',
                'created_at'   => date('Y-m-d H:i:s'),
                 'updated_at'   => date('Y-m-d H:i:s')         
                );
                $not_empty_rows++;
            // dd('if');
               }
                else{

            // $insert = [];
            // dd('else');
         
          }
               }
               // var_dump ('Total Rows: '.$total_rows.' and Saved Rows: '.$not_empty_rows);
               // die();
   $import_stats[] = array(
                 'file_title'  => $file_name  ?? 'N/A' ,
                 'total_rows'   => $total_rows ?? 'N/A'  ,
                 'saved_rows'   => $not_empty_rows ?? 'N/A' ,
                 'empty_rows'   => ($total_rows - $not_empty_rows)  ?? 'N/A',
                 'created_at'   => date('Y-m-d H:i:s'),
                 'updated_at'   => date('Y-m-d H:i:s')
                 
                );

              // }
            // dd('before insert');

      if(!empty($insert))
      {

            // dd('insert not empty');

       DB::table('customers')->insert($insert);

       DB::table('imports')->insert($import_stats);
       return response()->json(['status'=>1,'data'=> $import_stats,'message'=>$not_empty_rows.' Rows Added!']);               
      
      }     

      else{
            // dd('insert  empty');

        // return response()->json(['status'=>2,'message'=>'Data fields missing!']);               
      }

     // $request->session()->flash('alert-success', 'Excel Data Imported successfully');   



     // return back();


}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
      
        $customers = new Customer();
        $customers = $customer::with(['country','city','state'])->orderby('id','desc')->get();
       // echo '<pre>';
       //  var_dump($customers);
       // echo '<pre>';
        
       //  die();
        $countries = Country::all();
        return view('admin.customers',compact('customers','countries'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Customer $customer)
    {
        $id = $customer::with(['country','city','state'])->where('id',$request->customerId)->get();

        // $id = Customer::select('*')->where('id',$request->customerId)->get();

        return response()->json(['status'=>true,'data'=>$id]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
    }

    public function get_states(Request $request)
    {

       $states = State::where('country_id',$request->countryID)->get();
       return response()->json(['status' => 1,'data' => $states]);
    }
    
    public function get_cities(Request $request)
    {

       $cities = City::where('state_id',$request->stateID)->get();
       return response()->json(['status' => 1,'data' => $cities]);
    }

    


}

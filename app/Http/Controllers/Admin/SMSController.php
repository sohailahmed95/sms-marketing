<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Campaign;
use App\Country;
use App\Customer;
use App\State;
use App\City;
use App\Setting;
use App\ScheduleSMS;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;

class SMSController extends Controller
{

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
       $currentDateTime = date("Y-m-d H:i:s");
       $campaigns = Campaign::where(
        'status','=',1
        )->where(
        'start','>=',$currentDateTime
        )->where(
        'end','>',$currentDateTime
        )->get();


       // dd($campaigns[0]);

       // dd($currentDateTime);
       return view('admin.sendsms.send',compact('campaigns'));  
    }

    public function sendsms(Request $request, Customer $customer)
    {
        // $customers = Campaign::with('campaign')->get();

        // dd($request->all());

        try
        {
            $rules = [
                'campaign_id'=>'required|exists:campaign,id',
                // 'message'=>'required'
            ];

            $validator = Validator::make($request->all(),$rules);
            if($validator->fails()){
                return response()->json([
                    'status'=>'Error',
                    'message'=>$validator->errors()
                ],422);
            }

            $campaign = Campaign::find($request->campaign_id);
            // $currentDateTime = date("Y-m-d H:i:s");

            // $currentDateTime = date("Y-m-d H:i:s");
            // dd($campaign->start < date("Y-m-d H:i:s"));

            $users = Customer::where('registrant_country_id',$campaign->getcountry->id)
            ->where('registrant_state_id',$campaign->getstate->id)
            ->where('registrant_city_id',$campaign->getcity->id)
            ->where('status',1)
            ->get();
            $env = Setting::where('status',1)->first();
            $response = '';
            // dd($users->toArray());
            foreach($users as $user)
            {
                $message = new \Html2Text\Html2Text($campaign->description);
                // dd($html);

                $response = sendMessage($message->getText(),$user->registrant_phone,$env);
                // dd($response);
                if($response != null){
                
                if($response[0]->status == 'not sent')
                {
                    $response=1;
                }
                else if($response[0]->status == 'queued')
                {
                    $response=2;
                }
                else if($response[0]->status == 'sent')
                {

                    $response=3;
                }
                else if($response[0]->status == 'delivered')
                {
                    $response=4;
                }
                else{
                 $sms_exception =  Session::get('sms_exception');
                 $response = -1;
                }


                // dd(ScheduleSMS::all());
                ScheduleSMS::insert([
                    'campaign_id'=>$request->campaign_id?? null,
                    'customer_id'=>$user->id?? null,
                    'sms_status_id'=>$response?? null,
                    'created_at'=>date("Y-m-d H:i:s")?? null,
                    'sms_exception'=>$sms_exception ?? null
                ]);
            }
            else{
                     Session::flash('alert-danger','Error!!');

            }
            }
            if($response != null){
                if(count($users->toArray()) > 0){
                         Session::flash('alert-success','Campaign Started!');
                }
            }
             else{
                if(count($users->toArray()) <= 0){
                     Session::flash('alert-warning','No Active User For Selected Campaign');
                }
                else{
                     Session::flash('alert-danger','Error!!');
                }

             }
            return redirect('sms');
            // Session::flash('alert-success','SMS Successfully Sent');
            // return redirect('sms');

        }
        catch (Exception $exception) {

        }    
    }
}

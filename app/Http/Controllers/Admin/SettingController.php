<?php

namespace App\Http\Controllers\Admin;

use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{

      public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::all();
        $active_setting = Setting::where('status',1)->first();
        return view('admin.api_settings',compact('settings','active_setting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $settings = new Setting;
        $settings->auth_token = $request->auth_token;
        $settings->account_sid= $request->account_sid;
        $settings->twilio_no= $request->twilio_no;
        if ($settings->save()) {
            $request->session()->flash('alert-success', 'API Added');
            return back();
     
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        //
    }  


      public function change_api(Request $request, Setting $setting)
    {
       // dd($twilio_number);
        foreach ($setting->all() as $setting) {
            if ($setting->twilio_no == $request->active_twilio_number) {
                $setting->where('twilio_no','=',$request->active_twilio_number)->update(['status'=>1]);
            } 
            else
            {
                $setting->where('twilio_no','!=',$request->active_twilio_number)->update(['status'=>0]);
            }
        }
            // $env = $setting->where('status',1)->first();
            // putenv("TWILIO_SID=".$env->account_sid);
            // putenv("TWILIO_AUTH_TOKEN=".$env->auth_token);
            // putenv("TWILIO_NO=".$env->twilio_no);
              $request->session()->flash('alert-success', 'Active API Changed');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        // dd($id);
        $setting = Setting::findOrFail($id);
        $setting->delete();
            $request->session()->flash('alert-danger', 'API Removed');

        return back();
    }
}

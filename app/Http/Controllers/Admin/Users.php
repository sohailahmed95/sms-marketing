<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class Users extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    }

    public function users_list(){

   		$users = User::all();
        return view('admin.users',compact('users'));
       	// return view('layouts.admin.users');
    }

    public function status_update(Request $request)
    {   
     
      $user = User::where('id',$request->userId)->first();
        if ($user->is_active  == 0) {
        
          $user->is_active = 1;
          $user->save();
        } 
        elseif ($user->is_active  == 1) {
        
          $user->is_active = 0;
          $user->save();
        } 
      
        return response()->json(['status'=>true,'data'=>$user]);
    }
}

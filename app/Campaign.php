<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $table = 'campaign';

    protected $fillable = ['title','description','city_id','state_id','country_id','start','end','status'];

    public function getcity()
    {
    	return $this->hasOne('App\City','id','city_id');
    }

    public function getstate()
    {
    	return $this->hasOne('App\State','id','state_id');
    }

    public function getcountry()
    {
    	return $this->hasOne('App\Country','id','country_id');
    }

    public function sms_status(){
        return $this->hasMany('App\ScheduleSMS','id');
    }


}


<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = "city";
    protected $fillable = ['name','state_id'];

      public function state(){
    	return $this->belongsTo('App\State');
    }

    
      public function customer(){
    	return $this->belongsTo('App\Customer');
    }

   
}


